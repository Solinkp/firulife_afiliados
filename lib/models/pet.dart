class Pet {
  final String? id;
  final String businessId;
  final String name;
  final bool sex;
  final String age;
  final String pictureUrl;
  final String description;

  Pet({
    this.id,
    required this.businessId,
    required this.name,
    required this.sex,
    required this.age,
    required this.pictureUrl,
    required this.description
  });

  static Pet buildPet(String id, dynamic data) {
    return Pet(
      id: id,
      businessId: data['businessId'],
      name: data['name'],
      sex: data['sex'],
      age: data['age'],
      pictureUrl: data['pictureUrl'],
      description: data['description']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'businessId': businessId,
      'name': name,
      'sex': sex,
      'age': age,
      'pictureUrl': pictureUrl,
      'description': description
    };
  }

}

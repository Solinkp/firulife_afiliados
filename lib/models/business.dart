import 'package:flutter/material.dart';

class Business {
  final String? id;
  final bool active;
  final String? userId;
  final int type; // 0 = shelter | 1 = vet | 2 = store | 3 = walkers | 4 = indep. vet
  final String name;
  final List<Map<String, String>> addresses;
  final String pictureUrl;
  final List<String> phone;
  final List<String> services;
  final List<Map<String, String>> openingHours;
  final Map<String, List<Map<String, String>>>? donationAccounts;
  final List<Map<String, String>> contact;
  final String? adoptionLink;
  final Color? businessColor;
  final String countryId;

  Business({
    this.id,
    required this.active,
    required this.userId,
    required this.type,
    required this.pictureUrl,
    required this.name,
    required this.addresses,
    required this.phone,
    required this.openingHours,
    required this.services,
    required this.donationAccounts,
    required this.contact,
    required this.adoptionLink,
    required this.businessColor,
    required this.countryId
  });

  static Business buildBusiness(String id, dynamic data) {
    return Business(
      id: id,
      active: data['active'],
      userId: data['userId'] ?? '',
      type: data['type'],
      pictureUrl: data['pictureUrl'],
      name: data['name'],
      addresses: data['addresses'].cast<Map<String, String>>(),
      phone: data['phone'].cast<String>(),
      openingHours: data['openingHours'].cast<Map<String, String>>(),
      services: data['services'].cast<String>(),
      donationAccounts: data['donationAccounts'].cast<String, List<Map<String, String>>>(),
      contact: data['contact'].cast<Map<String, String>>(),
      adoptionLink: data['adoptionLink'],
      businessColor: data['businessColor'] != null ? Color(int.parse(data['businessColor'])) : null,
      countryId: data['countryId']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'active': active,
      'userId': userId,
      'type': type,
      'pictureUrl': pictureUrl,
      'name': name,
      'addresses': addresses.cast(),
      'phone': phone.cast(),
      'openingHours': openingHours.cast(),
      'services': services.cast(),
      'donationAccounts': donationAccounts == null ? {} : donationAccounts!.cast(),
      'contact': contact.cast(),
      'adoptionLink': adoptionLink,
      'businessColor': businessColor,
      'countryId': countryId
    };
  }

}

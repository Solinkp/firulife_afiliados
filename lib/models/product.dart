class Product {
  final String? id;
  final String businessId;
  final String name;
  final String currency;
  final double price;
  final String smallDescription;
  final String fullDescription;
  final String pictureUrl;

  Product({
    this.id,
    required this.businessId,
    required this.name,
    required this.currency,
    required this.price,
    required this.smallDescription,
    required this.fullDescription,
    required this.pictureUrl
  });

  static Product buildProduct(String id, dynamic data) {
    return Product(
      id: id,
      businessId: data['businessId'],
      name: data['name'],
      currency: data['currency'],
      price: data['price'].toDouble(),
      smallDescription: data['smallDescription'],
      fullDescription: data['fullDescription'],
      pictureUrl: data['pictureUrl']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'businessId': businessId,
      'name': name,
      'currency': currency,
      'price': price,
      'smallDescription': smallDescription,
      'fullDescription': fullDescription,
      'pictureUrl': pictureUrl
    };
  }

}

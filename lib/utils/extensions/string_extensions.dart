import 'package:flutter/material.dart';
import 'package:firulife_afiliados/generated/l10n.dart';

extension StringExtension on String {
  String? validateEmpty(BuildContext context) =>
      (isEmpty) ? 'S.of(context).required' : null;

  String? validateEmail(BuildContext context) {
    var regex = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return (isEmpty)
        ? S.of(context).form.required
        : (!regex.hasMatch(this))
            ? S.of(context).form.invalidEmail
            : null;
  }
}

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../services/authentication.dart';
import '../../settings/custom_colors.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../routing/fade_route.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_logo.dart';
import '../../widgets/forms/custom_text_form_field.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/forms/form_error_message.dart';
import './reset_password_screen.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final BaseAuth _auth = Auth();
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    CustomSharedPreferences().getLocalEmail().then((value) {
      if(mounted){
        setState(() {
          _emailController.text = value;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: Colors.white);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: Container(
          child: ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              FiruLogo(),
              Padding(padding: const EdgeInsets.only(top: 20.0)),
              CustomTextFormField(
                inputType: TextInputType.emailAddress,
                hintText: 'Correo',
                icon: Icons.mail,
                index: 0,
                setValueForm: _setValueForm,
                validatorFunction: _validatorFunction,
                textController: _emailController
              ),
              CustomTextFormField(
                inputType: TextInputType.text,
                obscured: true,
                hintText: 'Contraseña',
                icon: Icons.lock,
                index: 1,
                last: true,
                setValueForm: _setValueForm,
                validatorFunction: _validatorFunction,
                textController: _passwordController
              ),
              CustomButton(
                text: 'Iniciar Sesión',
                pressCallback: _validateAndSubmit
              ),
              FormErrorMessage(errorMessage: _errorMessage),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: 15, color: Colors.white, decoration: TextDecoration.underline),
                      text: 'Olvidé mi contraseña',
                      recognizer: TapGestureRecognizer()..onTap = _goToRecoverPassword
                    )
                  )
                )
              )
            ]
          )
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    if(index == 0) {
      _emailController.text = value.trim();
    } else {
      _passwordController.text = value.trim();
    }
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? 'Correo no puede estar vacío' : null;
    } else if(index == 1) {
      return value.toString().trim().isEmpty ? 'Contraseña no puede estar vacía' : null;
    }
    return null;
  }

  bool validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      await CustomSharedPreferences().setLocalEmail(_emailController.text);
      _auth.signIn(_emailController.text, _passwordController.text).then((result) {
        if(result != null) {
          if(mounted) {
            setState(() {
              _isLoading = false;
              _errorMessage = result;
            });
          }
        }
      }).catchError(_onSignInError);
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  _onSignInError(e) {
    if(mounted){
      String message = '';
      switch(e.code) {
        case 'invalid-email':
          message = 'Correo mal escrito.';
          break;
        case 'wrong-password':
          message = 'Contraseña equivocada.';
          break;
        case 'user-not-found':
          message = 'No se encontró ningún usuario con el correo ingresado.';
          break;
        case 'too-many-requests':
          message = 'Acceso bloqueado temporalmente debido a muchos intentos fallidos, por favor intente de nuevo más tarde.';
          break;
        case 'user-disabled':
          message = 'Este usuario ha sido deshabilitado por un administrador.';
          break;
        default:
          message = e.message;
      }
      setState(() {
        _isLoading = false;
        _errorMessage = message;
      });
    }
  }

  void _goToRecoverPassword() {
    Navigator.of(context).push(FadeRoute(page: ResetPasswordScreen()));
  }

}

import 'package:flutter/material.dart';

import '../../services/authentication.dart';
import '../../settings/custom_colors.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_logo.dart';
import '../../widgets/forms/custom_text_form_field.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/forms/form_error_message.dart';

class ResetPasswordScreen extends StatefulWidget {
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final BaseAuth _auth = Auth();
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(elevation: 0),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: Colors.white);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: Container(
          child: ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              FiruLogo(),
              Padding(padding: const EdgeInsets.only(top: 20.0)),
              CustomTextFormField(
                inputType: TextInputType.emailAddress,
                hintText: 'Correo',
                icon: Icons.mail,
                index: 0,
                last: true,
                setValueForm: _setValueForm,
                validatorFunction: _validatorFunction,
                textController: _emailController
              ),
              CustomButton(
                text: 'Recuperar Contraseña',
                pressCallback: _validateAndSubmit
              ),
              FormErrorMessage(errorMessage: _errorMessage)
            ]
          )
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    if(index == 0) {
      _emailController.text = value.trim();
    }
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? 'Correo no puede estar vacío' : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = '';
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _auth.sendPasswordResetEmail(_emailController.text).then((_) {
        setState(() {
          _isLoading = false;
        });
        _formKey.currentState?.reset();
        _showSnack('Te hemos enviado un correo para recuperar tu contraseña, revisa tu bandeja de entrada.');
      }).catchError(_onResetPasswordError);
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _showSnack(String text) {
    final messenger = ScaffoldMessenger.of(context);
    SnackBar snackBar = SnackBar(
      backgroundColor: CustomColors.mainDark,
      content: Text(text),
      action: SnackBarAction(
        label: 'X',
        textColor: Colors.white,
        onPressed: () => messenger.hideCurrentSnackBar()
      )
    );
    messenger.showSnackBar(snackBar);
  }

  _onResetPasswordError(e) {
    if(mounted) {
      String message = '';
      switch(e.code) {
        case 'invalid-email':
          message = 'Correo mal escrito.';
          break;
        case 'user-not-found':
          message = 'No se encontró ningún usuario con el correo ingresado.';
          break;
        case 'user-disabled':
          message = 'Este usuario ha sido deshabilitado por un administrador.';
          break;
        default:
          message = 'Ocurrió un problema, por favor intenta de nuevo más tarde.';
      }
      setState(() {
        _isLoading = false;
        _errorMessage = message;
      });
    }
  }

}

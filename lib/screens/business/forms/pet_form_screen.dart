import 'package:flutter/material.dart';

import '../../../models/pet.dart';
import '../../../settings/tldr.dart' show PetFormTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/forms/pet_form.dart';

class PetFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<dynamic> _args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    bool _isNew = _args[0];
    Pet? _pet = _args[1];

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(_isNew ? PetFormTexts.newTitle : PetFormTexts.editTitle, style: Theme.of(context).textTheme.headline6)
        ),
        body: PetForm(pet: _pet)
      )
    );
  }
}

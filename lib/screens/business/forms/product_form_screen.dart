import 'package:flutter/material.dart';

import '../../../models/product.dart';
import '../../../settings/tldr.dart' show ProductFormTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/forms/product_form.dart';

class ProductFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<dynamic> _args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    bool _isNew = _args[0];
    Product? _product = _args[1];

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(_isNew ? ProductFormTexts.newTitle : ProductFormTexts.editTitle, style: Theme.of(context).textTheme.headline6)
        ),
        body: ProductForm(product: _product)
      )
    );
  }
}

import 'package:flutter/material.dart';

import '../../../models/business.dart';
import '../../../settings/tldr.dart' show AccountFormTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/forms/account_form.dart';

class AccountFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<dynamic> _args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    bool _isNew = _args[0];
    String _bankKey = _args[1];
    Business _business = _args[2];
    int? _index = _args[3];

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(_isNew ? AccountFormTexts.newTitle : AccountFormTexts.editTitle, style: Theme.of(context).textTheme.headline6)
        ),
        body: AccountForm(_bankKey, _business, _index)
      )
    );
  }
}

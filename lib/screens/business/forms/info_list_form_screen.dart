import 'package:flutter/material.dart';

import '../../../models/business.dart';
import '../../../settings/tldr.dart' show InfoFormScreenTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/forms/address_form.dart';
import '../../../widgets/forms/phone_form.dart';
import '../../../widgets/forms/service_form.dart';

class InfoListFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<dynamic> _args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    final bool _isNew = _args[0];
    final int _listType = _args[1];
    final Business _business = _args[2];
    final int? _itemIndex = _args[3];
    String _title = '';

    switch (_listType) {
      case 0:
        if(_isNew) {
          _title = '${InfoFormScreenTexts.nuevaTitle} ${InfoFormScreenTexts.addressTitle}';
        } else {
          _title = '${InfoFormScreenTexts.editTitle} ${InfoFormScreenTexts.addressTitle}';
        }
        break;
      case 1:
        if(_isNew) {
          _title = '${InfoFormScreenTexts.nuevoTitle} ${InfoFormScreenTexts.phoneTitle}';
        } else {
          _title = '${InfoFormScreenTexts.editTitle} ${InfoFormScreenTexts.phoneTitle}';
        }
        break;
      case 3:
        if(_isNew) {
          _title = '${InfoFormScreenTexts.nuevoTitle} ${InfoFormScreenTexts.serviceTitle}';
        } else {
          _title = '${InfoFormScreenTexts.editTitle} ${InfoFormScreenTexts.serviceTitle}';
        }
        break;
    }

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(_title, style: Theme.of(context).textTheme.headline6)
        ),
        body: _getBodyForm(_listType, _business, _itemIndex)
      )
    );
  }

  Widget _getBodyForm(int listType, Business business, int? itemIndex) {
    return listType == 0 ? AddressForm(business, itemIndex)
    : listType == 1 ? PhoneForm(business, itemIndex)
    : ServiceForm(business, itemIndex);
  }

}

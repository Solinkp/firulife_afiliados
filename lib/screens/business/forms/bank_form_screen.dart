import 'package:flutter/material.dart';

import '../../../settings/tldr.dart' show BankFormTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/forms/bank_form.dart';

class BankFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<dynamic> _args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    bool _isNew = _args[0];
    String? _bankKey = _args[1];

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(_isNew ? BankFormTexts.newTitle : BankFormTexts.editTitle, style: Theme.of(context).textTheme.headline6)
        ),
        body: BankForm(_bankKey)
      )
    );
  }
}

import 'package:flutter/material.dart';

import '../../../models/business.dart';
import '../../../settings/tldr.dart' show InfoSegmentScreenTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/forms/schedule_form.dart';
import '../../../widgets/forms/contact_form.dart';

class SegmentFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<dynamic> _args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    final int _segmentIndex = _args[0];
    final Business _business = _args[1];
    String _title = '';

    if(_segmentIndex == 2) {
      _title = InfoSegmentScreenTexts.scheduleTitle;
    } else {
      _title = InfoSegmentScreenTexts.contactTitle;
    }

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(_title, style: Theme.of(context).textTheme.headline6)
        ),
        body: _segmentIndex == 2 ? ScheduleForm(_business) : ContactForm(_business)
      )
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../routing/fade_route.dart';
import '../../services/authentication.dart';
import '../../settings/custom_colors.dart';
import '../../settings/pop_up_menu_options.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/custom_text_button.dart';
import '../../widgets/dashboard.dart';
import '../app_info_screen.dart';

class BusinessDashboard extends StatelessWidget {
  final BaseAuth _auth = Auth();

  @override
  Widget build(BuildContext context) {
    User? _user = Provider.of<User?>(context);

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Bienvenid@ ${_user!.displayName}', style: Theme.of(context).textTheme.headline6),
          actions: [
            PopupMenuButton(
              color: CustomColors.mainDark,
              onSelected: (choice) => _choiceAction(context, choice.toString(), _user.email),
              itemBuilder: (BuildContext context) {
                return PopUpMenuOptions.options.map((String option) {
                  return PopupMenuItem(
                    value: option,
                    child: Text(option, style: TextStyle(color: Colors.white))
                  );
                }).toList();
              }
            )
          ]
        ),
        body: Dashboard()
      )
    );
  }

  void _choiceAction(BuildContext context, String choice, String? email) {
    if(choice == PopUpMenuOptions.about) {
      Navigator.of(context).push(FadeRoute(page: AppInfoScreen()));
    }
    if(choice == PopUpMenuOptions.password) {
      _showPasswordChangeAlert(context, email!);
    }
    if(choice == PopUpMenuOptions.signout) {
      _showLogOutAlert(context);
    }
  }

  void _showPasswordChangeAlert(BuildContext context, String email) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro/a que desea cambiar su contraseña?')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Cambiar Contraseña',
              pressCallback: () {
                _auth.sendPasswordResetEmail(email);
                _showSnack(context, 'Te hemos enviado un correo para cambiar tu contraseña.');
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showSnack(BuildContext context, String text) {
    final messenger = ScaffoldMessenger.of(context);
    SnackBar snackBar = SnackBar(
      backgroundColor: CustomColors.mainDark,
      content: Text(text),
      action: SnackBarAction(
        label: 'X',
        textColor: Colors.white,
        onPressed: () => messenger.hideCurrentSnackBar()
      )
    );
    messenger.showSnackBar(snackBar);
  }

  void _showLogOutAlert(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro/a que desea cerrar sesión?')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Cerrar sesión',
              pressCallback: () {
                Navigator.of(context).pop();
                _auth.signOut();
              }
            )
          ]
        );
      }
    );
  }

}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../../../services/database.dart';
import '../../../models/business.dart';
import '../../../settings/custom_colors.dart';
import '../../../settings/tldr.dart' show InfoTexts;
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/spinner_loader.dart';
import '../../../widgets/forms/form_image_picker.dart';
import '../../../widgets/custom_text_button.dart';
import '../../../widgets/info_panel.dart';

class InfoScreen extends StatefulWidget {
  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  final BaseDatabase _database = Database();
  Business? _business;
  File? _picture;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    _business = Provider.of<Business?>(context);

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('${_business?.name}', style: Theme.of(context).textTheme.headline6)
        ),
        body: _business == null ? SpinnerLoader(
          color: CustomColors.mainGreen
        ) : Container(
          child: Stack(children: [
            ListView(
              shrinkWrap: true,
              children: [
                Padding(padding: const EdgeInsets.only(top: 10.0)),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [FormImagePicker(
                    picture: _picture,
                    pictureUrl: _business!.pictureUrl,
                    showPicturePicker: _showPicturePicker,
                    removePicture: null,
                    type: 0
                  )]
                ),
                InfoPanel(InfoTexts.panelAddresses, 0),
                InfoPanel(InfoTexts.panelPhones, 1),
                InfoPanel(InfoTexts.panelSchedule, 2),
                InfoPanel(InfoTexts.panelServices, 3),
                InfoPanel(InfoTexts.panelContact, 4)
              ]
            ),
            _showLoader()
          ])
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  void _showPicturePicker() {
    if(_isLoading) {
      return;
    }
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: InfoTexts.changeLogoTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: InfoTexts.changeLogoBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: InfoTexts.changeLogoCancel,
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: InfoTexts.changeLogoAccept,
              pressCallback: () {
                Navigator.of(context).pop();
                _getImageFromSource(ImageSource.gallery);
              }
            )
          ]
        );
      }
    );
  }

  void _getImageFromSource(ImageSource source) async {
    final ImagePicker picker = ImagePicker();
    PickedFile? image = await picker.getImage(
      source: source,
      imageQuality: 50
    );
    if(image != null) {
      setState(() {
        _picture = File(image.path);
      });
      _saveNewLogo();
    }
  }

  void _saveNewLogo() async {
    setState(() {
      _isLoading = true;
    });
    _database.deleteBusinessImage(_business!.type, _business!.pictureUrl).then((success) {
      if(success) {
        _database.uploadBusinessImage(_business!.type, _business!.name, _picture!).then((newPicUrl) {
          Business editedBusiness = Business(
            active: _business!.active,
            addresses: _business!.addresses,
            adoptionLink: _business!.adoptionLink,
            businessColor: _business!.businessColor,
            contact: _business!.contact,
            countryId: _business!.countryId,
            donationAccounts: _business!.donationAccounts,
            name: _business!.name,
            openingHours: _business!.openingHours,
            phone: _business!.phone,
            services: _business!.services,
            type: _business!.type,
            userId: _business!.userId,
            id: _business!.id,
            pictureUrl: newPicUrl
          );
          _database.saveBusiness(editedBusiness, false).then((success) {
            if(success) {
              setState(() {
                _isLoading = false;
              });
            }
          }).catchError((e) {print('error save business = $e');});
        }).catchError((e) {print('error upload pic = $e');});
      }
    }).catchError((e) {print('error delete pic = $e');});
  }

}

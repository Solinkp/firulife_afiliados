import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../routing/fade_route.dart';
import '../../../services/database.dart';
import '../../../models/business.dart';
import '../../../settings/tldr.dart' show BankTexts;
import '../../../settings/custom_colors.dart';
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/custom_text_button.dart';
import '../../../widgets/spinner_loader.dart';
import '../forms/bank_form_screen.dart';
import './accounts_screen.dart';

class BanksScreen extends StatelessWidget {
  final BaseDatabase _database = Database();

  @override
  Widget build(BuildContext context) {
    Business? _business = Provider.of<Business?>(context);

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(BankTexts.title, style: Theme.of(context).textTheme.headline6)
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.of(context).push(FadeRoute(page: BankFormScreen(), arguments: [true, null])),
          backgroundColor: CustomColors.mainDark,
          child: Icon(Icons.add, color: Colors.white, size: 28)
        ),
        body: _business == null ? SpinnerLoader(
          color: CustomColors.mainGreen
        ) : _business.donationAccounts!.length == 0 ? Container(
          alignment: Alignment.center,
          child: Text(
            BankTexts.noBanksFound,
            style: TextStyle(fontSize: 20, color: CustomColors.mainDark),
            textAlign: TextAlign.center
          )
        ) : SingleChildScrollView(
          child: Container(
            alignment: Alignment.topCenter,
            child: DataTable(
              columns: [
                DataColumn(label: Expanded(child: Text(BankTexts.tableColumnOne, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
                DataColumn(label: Expanded(child: Text(BankTexts.tableColumnTwo, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center)))
              ],
              rows: _getTableRows(context, _business)
            )
          ),
        )
      )
    );
  }

  List<DataRow> _getTableRows(BuildContext context, Business business) {
    List donationKeys = business.donationAccounts!.keys.toList();
    return donationKeys.map((key) =>
      DataRow(cells: [
        DataCell(Center(child: Text(key, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
        DataCell(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(tooltip: BankTexts.btnShowAccounts, icon: Icon(Icons.remove_red_eye, color: CustomColors.mainGreen), onPressed: () => _openDonationAccountDetails(context, key)),
              IconButton(tooltip: BankTexts.btnEdit, icon: Icon(Icons.edit, color: CustomColors.mainGreen), onPressed: () => _editItem(context, key)),
              IconButton(tooltip: BankTexts.btnDelete, icon: Icon(Icons.delete, color: CustomColors.mainDark), onPressed: () => _deleteItem(context, business, key))
            ]
          )
        )
      ])
    ).toList();
  }

  void _openDonationAccountDetails(BuildContext context, String bankKey) {
    Navigator.of(context).push(FadeRoute(page: AccountsScreen(), arguments: bankKey));
  }

  void _editItem(BuildContext context, String bankKey) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: BankTexts.editTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: BankTexts.editBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: BankTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: BankTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: BankFormScreen(), arguments: [false, bankKey]));
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, Business business, String bankKey) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: BankTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: BankTexts.deleteBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: BankTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: BankTexts.btnDelete,
              pressCallback: () async {
                Map donationAccounts = business.donationAccounts!.cast();
                donationAccounts.remove(bankKey);
                bool success = await _database.saveBusiness(business, false);
                if(success) {
                  Navigator.of(context).pop();
                }
              }
            )
          ]
        );
      }
    );
  }

}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../routing/fade_route.dart';
import '../../../services/database.dart';
import '../../../models/pet.dart';
import '../../../models/business.dart';
import '../../../settings/tldr.dart' show PetTexts;
import '../../../settings/custom_colors.dart';
import '../../../settings/custom_shared_preferences.dart';
import '../../../settings/placeholder_images.dart';
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/spinner_loader.dart';
import '../../../widgets/custom_text_button.dart';
import '../forms/pet_form_screen.dart';

class PetsScreen extends StatefulWidget {
  @override
  _PetsScreenState createState() => _PetsScreenState();
}

class _PetsScreenState extends State<PetsScreen> {
  final BaseDatabase _database = Database();
  String _businessId = '';

  @override
  void initState() {
    CustomSharedPreferences().getLocalBusiness().then((value) {
      setState(() {
        _businessId = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(PetTexts.title, style: Theme.of(context).textTheme.headline6)
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.of(context).push(FadeRoute(page: PetFormScreen(), arguments: [true, null])),
          backgroundColor: CustomColors.mainDark,
          child: Icon(Icons.add, color: Colors.white, size: 28)
        ),
        body: StreamProvider<List<Pet>?>.value(
          value: _database.getBusinessPets(_businessId),
          initialData: null,
          updateShouldNotify: (_, __) => true,
          catchError: (_, object) => null,
          child: PetList()
        )
      )
    );
  }
}

class PetList extends StatelessWidget {
  final BaseDatabase _database = Database();
  final String _placeholderImage = PlaceholderImages.placeholderFiru;

  @override
  Widget build(BuildContext context) {
    List<Pet>? _petList = Provider.of<List<Pet>?>(context);
    int? _businessType = Provider.of<Business?>(context)?.type;

    return _petList == null ? SpinnerLoader(
      color: CustomColors.mainGreen
    ) : _petList.length == 0 ? Container(
      alignment: Alignment.center,
      child: Text(
        PetTexts.noPetsFound,
        style: TextStyle(fontSize: 20, color: CustomColors.mainDark),
        textAlign: TextAlign.center
      )
    ) : SingleChildScrollView(
      child: Container(
        alignment: Alignment.topCenter,
        child: DataTable(
          columns: [
            DataColumn(label: Expanded(child: Text(PetTexts.tableColumnOne, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
            DataColumn(label: Expanded(child: Text(PetTexts.tableColumnTwo, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center)))
          ],
          rows: _getTableRows(context, _petList, _businessType!),
        )
      )
    );
  }

  List<DataRow> _getTableRows(BuildContext context, List<Pet> petList, int businessType) {
    return petList.map((pet) =>
      DataRow(cells: [
        DataCell(Center(child: Text(pet.name, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
        DataCell(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(tooltip: PetTexts.btnShowDetails, icon: Icon(Icons.remove_red_eye, color: CustomColors.mainGreen), onPressed: () => _openPetDetails(context, pet)),
              IconButton(tooltip: PetTexts.btnEdit, icon: Icon(Icons.edit, color: CustomColors.mainGreen), onPressed: () => _editItem(context, pet)),
              IconButton(tooltip: PetTexts.btnDelete, icon: Icon(Icons.delete, color: CustomColors.mainDark), onPressed: () => _deleteItem(context, pet, businessType))
            ]
          )
        )
      ])
    ).toList();
  }

  void _openPetDetails(BuildContext context, Pet pet) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Información de Mascota\n\n', style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)),
                Hero(
                  tag: 'pet_${pet.id}',
                  child: FadeInImage(
                    height: 250,
                    fit: BoxFit.contain,
                    placeholder: AssetImage(_placeholderImage),
                    image: NetworkImage(pet.pictureUrl),
                    imageErrorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
                      return Image.asset(_placeholderImage);
                    }
                  )
                ),
                Padding(padding: EdgeInsets.all(15)),
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                    style: TextStyle(fontSize: 16, color: Colors.white),
                    children: <TextSpan>[
                      TextSpan(text: 'Nombre: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.name}\n\n'),
                      TextSpan(text: 'Género: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.sex ? 'Macho':'Hembra'}\n\n'),
                      TextSpan(text: 'Nacimiento: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.age}\n\n'),
                      TextSpan(text: 'Descripción: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.description}')
                    ]
                  )
                )
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PetTexts.btnClose,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: PetTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: PetFormScreen(), arguments: [false, pet]));
              }
            )
          ]
        );
      }
    );
  }

  void _editItem(BuildContext context, Pet pet) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: PetTexts.editTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: PetTexts.editBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PetTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: PetTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: PetFormScreen(), arguments: [false, pet]));
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, Pet pet, int businessType) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: PetTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: PetTexts.deleteBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PetTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: PetTexts.btnDelete,
              pressCallback: () async {
                bool success = await _database.deletePet(businessType, pet.id!, pet.pictureUrl);
                if(success) {
                  Navigator.of(context).pop();
                }
              }
            )
          ]
        );
      }
    );
  }

}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../routing/fade_route.dart';
import '../../../services/database.dart';
import '../../../models/product.dart';
import '../../../models/business.dart';
import '../../../settings/tldr.dart' show ProductTexts;
import '../../../settings/custom_colors.dart';
import '../../../settings/custom_shared_preferences.dart';
import '../../../settings/placeholder_images.dart';
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/spinner_loader.dart';
import '../../../widgets/custom_text_button.dart';
import '../forms/product_form_screen.dart';

class ProductsScreen extends StatefulWidget {
  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  final BaseDatabase _database = Database();
  String _businessId = '';

  @override
  void initState() {
    CustomSharedPreferences().getLocalBusiness().then((value) {
      setState(() {
        _businessId = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(ProductTexts.title, style: Theme.of(context).textTheme.headline6)
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.of(context).push(FadeRoute(page: ProductFormScreen(), arguments: [true, null])),
          backgroundColor: CustomColors.mainDark,
          child: Icon(Icons.add, color: Colors.white, size: 28)
        ),
        body: StreamProvider<List<Product>?>.value(
          value: _database.getBusinessProducts(_businessId),
          initialData: null,
          updateShouldNotify: (_, __) => true,
          catchError: (_, object) => null,
          child: ProductList()
        )
      )
    );
  }
}

class ProductList extends StatelessWidget {
  final BaseDatabase _database = Database();
  final String _placeholderImage = PlaceholderImages.placeholderProduct;

  @override
  Widget build(BuildContext context) {
    List<Product>? _productList = Provider.of<List<Product>?>(context);
    int? _businessType = Provider.of<Business?>(context)?.type;

    return _productList == null ? SpinnerLoader(
      color: CustomColors.mainGreen
    ) : _productList.length == 0 ? Container(
      alignment: Alignment.center,
      child: Text(
        ProductTexts.noProductsFound,
        style: TextStyle(fontSize: 20, color: CustomColors.mainDark),
        textAlign: TextAlign.center
      )
    ) : SingleChildScrollView(
      child: Container(
        alignment: Alignment.topCenter,
        child: DataTable(
          columns: [
            DataColumn(label: Expanded(child: Text(ProductTexts.tableColumnOne, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
            DataColumn(label: Expanded(child: Text(ProductTexts.tableColumnTwo, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center)))
          ],
          rows: _getTableRows(context, _productList, _businessType!),
        )
      )
    );
  }

  List<DataRow> _getTableRows(BuildContext context, List<Product> productList, int businessType) {
    return productList.map((product) =>
      DataRow(cells: [
        DataCell(Center(child: Text(product.name, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
        DataCell(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(tooltip: ProductTexts.btnShowDetails, icon: Icon(Icons.remove_red_eye, color: CustomColors.mainGreen), onPressed: () => _openProductDetails(context, product)),
              IconButton(tooltip: ProductTexts.btnEdit, icon: Icon(Icons.edit, color: CustomColors.mainGreen), onPressed: () => _editItem(context, product)),
              IconButton(tooltip: ProductTexts.btnDelete, icon: Icon(Icons.delete, color: CustomColors.mainDark), onPressed: () => _deleteItem(context, product, businessType))
            ]
          )
        )
      ])
    ).toList();
  }

  void _openProductDetails(BuildContext context, Product product) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Información de Producto\n\n', style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)),
                Hero(
                  tag: 'product_${product.id}',
                  child: FadeInImage(
                    height: 250,
                    fit: BoxFit.contain,
                    placeholder: AssetImage(_placeholderImage),
                    image: NetworkImage(product.pictureUrl),
                    imageErrorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
                      return Image.asset(_placeholderImage);
                    }
                  )
                ),
                Padding(padding: EdgeInsets.all(15)),
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                    style: TextStyle(fontSize: 16, color: Colors.white),
                    children: <TextSpan>[
                      TextSpan(text: 'Nombre: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.name}\n\n'),
                      TextSpan(text: 'Precio: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.currency}${product.price}\n\n'),
                      TextSpan(text: 'Descripción Corta: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.smallDescription}\n\n'),
                      TextSpan(text: 'Descripción Completa: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.fullDescription}')
                    ]
                  )
                )
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ProductTexts.btnClose,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: ProductTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: ProductFormScreen(), arguments: [false, product]));
              }
            )
          ]
        );
      }
    );
  }

  void _editItem(BuildContext context, Product product) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ProductTexts.editTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ProductTexts.editBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ProductTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: ProductTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: ProductFormScreen(), arguments: [false, product]));
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, Product product, int businessType) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ProductTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ProductTexts.deleteBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ProductTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: ProductTexts.btnDelete,
              pressCallback: () async {
                bool success = await _database.deleteProduct(businessType, product.id!, product.pictureUrl);
                if(success) {
                  Navigator.of(context).pop();
                }
              }
            )
          ]
        );
      }
    );
  }

}

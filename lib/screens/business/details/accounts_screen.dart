import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../routing/fade_route.dart';
import '../../../services/database.dart';
import '../../../models/business.dart';
import '../../../settings/tldr.dart' show AccountTexts;
import '../../../settings/custom_colors.dart';
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/custom_text_button.dart';
import '../../../widgets/spinner_loader.dart';
import '../forms/account_form_screen.dart';

class AccountsScreen extends StatelessWidget {
  final BaseDatabase _database = Database();

  @override
  Widget build(BuildContext context) {
    Business? _business = Provider.of<Business?>(context);
    final String _bankKey = ModalRoute.of(context)!.settings.arguments as String;

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(AccountTexts.title, style: Theme.of(context).textTheme.headline6)
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.of(context).push(FadeRoute(page: AccountFormScreen(), arguments: [true, _bankKey, _business, null])),
          backgroundColor: CustomColors.mainDark,
          child: Icon(Icons.add, color: Colors.white, size: 28)
        ),
        body: _business == null ? SpinnerLoader(
          color: CustomColors.mainGreen
        ) : _business.donationAccounts!.cast()[_bankKey].length == 0 ? Container(
          alignment: Alignment.center,
          child: Text(
            AccountTexts.noAccountsFound,
            style: TextStyle(fontSize: 20, color: CustomColors.mainDark),
            textAlign: TextAlign.center
          )
        ) : SingleChildScrollView(
          child: Container(
            alignment: Alignment.topCenter,
            child: DataTable(
              columns: [
                DataColumn(label: Expanded(child: Text(AccountTexts.tableColumnOne, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
                DataColumn(label: Expanded(child: Text(AccountTexts.tableColumnTwo, style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center)))
              ],
              rows: _getTableRows(context, _business, _bankKey)
            )
          ),
        )
      )
    );
  }

  List<DataRow> _getTableRows(BuildContext context, Business business, String bankKey) {
    List accounts = business.donationAccounts!.cast()[bankKey];
    List<DataRow> rows = [];
    for(int i = 0; i < accounts.length; i ++) {
      String key = accounts[i].keys.first.toString();
      String detail = accounts[i].values.first.toString();
      rows.add(
        DataRow(cells: [
          DataCell(Center(child: Text('$key: $detail', style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.center))),
          DataCell(
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(tooltip: AccountTexts.btnEdit, icon: Icon(Icons.edit, color: CustomColors.mainGreen), onPressed: () => _editItem(context, bankKey, business, i)),
                IconButton(tooltip: AccountTexts.btnDelete, icon: Icon(Icons.delete, color: CustomColors.mainDark), onPressed: () => _deleteItem(context, bankKey, business, i))
              ]
            )
          )
        ])
      );
    }
    return rows;
  }

  void _editItem(BuildContext context, String bankKey, Business business, int index) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: AccountTexts.editTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: AccountTexts.editBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: AccountTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: AccountTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: AccountFormScreen(), arguments: [false, bankKey, business, index]));
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, String bankKey, Business business, int index) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: AccountTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: AccountTexts.deleteBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: AccountTexts.btnCancel,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            ),
            CustomTextButton(
              text: AccountTexts.btnDelete,
              pressCallback: () async {
                Map donationAccounts = business.donationAccounts!.cast();
                List accounts = donationAccounts[bankKey];
                accounts.removeAt(index);
                bool success = await _database.saveBusiness(business, false);
                if(success) {
                  Navigator.of(context).pop();
                }
              }
            )
          ]
        );
      }
    );
  }

}

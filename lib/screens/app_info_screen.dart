import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show AppInfoTexts;
import '../widgets/colored_safe_area.dart';
import '../widgets/firu_logo.dart';
import '../widgets/contact_icon.dart';

class AppInfoScreen extends StatefulWidget {
  @override
  _AppInfoScreenState createState() => _AppInfoScreenState();
}

class _AppInfoScreenState extends State<AppInfoScreen> {
  String _version = '';

  @override
  void initState() {
    _initPackageInfo();
    super.initState();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _version = info.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(AppInfoTexts.title, style: Theme.of(context).textTheme.headline6),
          elevation: 0
        ),
        body: Container(
          color: CustomColors.mainGreen,
          child: Column(children: [
            Expanded(
              flex: 2,
              child: FiruLogo()
            ),
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(15),
                alignment: Alignment.center,
                child: Scrollbar(
                  child: SingleChildScrollView(
                    child: Text(
                      AppInfoTexts.description,
                      style: const TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center
                    )
                  )
                )
              )
            ),
            Expanded(
              flex: 2,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: Column(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(
                        width: 2,
                        color: CustomColors.secondaryGreen
                      ))
                    ),
                    child: Text(AppInfoTexts.support)
                  ),
                  Padding(padding: EdgeInsets.only(top: 5.0)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ContactIcon(
                        contact: AppInfoTexts.whatsapp,
                        url: AppInfoTexts.whatsappNumber,
                        isPhone: true,
                        personalizedColor: CustomColors.secondaryGreen
                      ),
                      ContactIcon(
                        contact: AppInfoTexts.email,
                        url: AppInfoTexts.emailAddress,
                        isPhone: false,
                        personalizedColor: CustomColors.secondaryGreen
                      )
                    ]
                  )
                ])
              )
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  '${AppInfoTexts.version} $_version',
                  style: const TextStyle(fontSize: 15, color: CustomColors.secondaryGreen, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center
                )
              )
            )
          ])
        )
      )
    );
  }
}

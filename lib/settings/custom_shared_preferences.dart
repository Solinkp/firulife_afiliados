import 'package:shared_preferences/shared_preferences.dart';

class CustomSharedPreferences {
  final String _prefUserEmail = "email";
  final String _prefUserBusiness = "business";

  Future<String> getLocalEmail() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(_prefUserEmail) ?? '';
  }

  Future<String> getLocalBusiness() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(_prefUserBusiness) ?? '';
  }

  Future<bool> setLocalEmail(String value) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.setString(_prefUserEmail, value);
  }

  Future<bool> setLocalBusiness(String value) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.setString(_prefUserBusiness, value);
  }

}

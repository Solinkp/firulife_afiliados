class PopUpMenuOptions {
  static const String about = "Sobre Firulife Afiliados";
  static const String password = "Cambiar contraseña";
  static const String signout = "Cerrar sesión";

  static const List<String> options = [
    about,
    password,
    signout
  ];
}

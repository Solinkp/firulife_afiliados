class AppInfoTexts {
  static String title = 'Sobre Firulife Afiliados';
  static String description = 'Firulife Afiliados es la aplicación para '+
  'administrar tu refugio / veterinaria / tienda, dentro de la plataforma. '+
  'Toda la información que modifiques se verá reflejada a los usuarios de Firulife.';
  static String support = 'Soporte';
  static String whatsapp = 'whatsapp';
  static String whatsappNumber = '+50576145496';
  static String email = 'email';
  static String emailAddress = 'firulife.dev@gmail.com';
  static String version = 'Versión:';
}

class InfoTexts {
  static String changeLogoTitle = 'Cambiar Logo\n\n';
  static String changeLogoBody = '¿Está seguro que desea cambiar el logo?\n\nSe actualizará automáticamente al seleccionar un logo nuevo.';
  static String changeLogoCancel = 'Cancelar';
  static String changeLogoAccept = 'Cambiar';

  static String panelAddresses = 'Direcciones';
  static String panelPhones = 'Teléfonos';
  static String panelSchedule = 'Horarios de atención';
  static String panelServices = 'Servicios';
  static String panelContact = 'Redes de contacto';
}

class InfoPanelTexts {
  static String newAddress = 'Agregar nueva dirección';
  static String newPhone = 'Agregar nuevo teléfono';
  static String editSchedule = 'Editar horarios de atención';
  static String newService = 'Agregar nuevo servicio';
  static String editContact = 'Editar redes de contacto';

  static String tooltipEditAdd = 'Editar Dirección';
  static String tooltipDeleteAdd = 'Eliminar Dirección';

  static String tooltipEditPhone = 'Editar Teléfono';
  static String tooltipDeletePhone = 'Eliminar Teléfono';

  static String tooltipEditService = 'Editar Servicio';
  static String tooltipDeleteService = 'Eliminar Servicio';

  static String editTitle = 'Editar Registro\n\n';
  static String editBody = '¿Está seguro que desea editar el registro?';
  static String deleteTitle = 'Eliminar Registro\n\n';
  static String deleteBody = '¿Está seguro que desea eliminar el registro?';
  static String btnCancel = 'Cancelar';
  static String btnEdit = 'Editar';
  static String btnDelete = 'Eliminar';
}

class InfoFormScreenTexts {
  static String nuevoTitle = 'Nuevo';
  static String nuevaTitle = 'Nueva';
  static String editTitle = 'Editar';
  static String addressTitle = 'Dirección';
  static String phoneTitle = 'Teléfono';
  static String serviceTitle = 'Servicio';
}

class AddresFormTexts {
  static String addressField = 'Dirección';
  static String coordinatesField = 'Coordenadas';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = '¡Dirección registrada!\n\n';
  static String successBody = 'Tu dirección se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'Tu dirección no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class PhoneFormTexts {
  static String phoneField = 'Teléfono';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = 'Teléfono registrado!\n\n';
  static String successBody = 'Tu teléfono se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'Tu teléfono no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class ServiceFormTexts {
  static String serviceField = 'Servicio';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = 'Servicio registrado!\n\n';
  static String successBody = 'Tu servicio se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'Tu servicio no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class InfoSegmentScreenTexts {
  static String scheduleTitle = 'Editar horarios de atención';
  static String contactTitle = 'Editar redes de contacto';
}

class ScheduleFormTexts {
  static String monField = 'Lunes';
  static String tueField = 'Martes';
  static String wedField = 'Miércoles';
  static String thuField = 'Jueves';
  static String friField = 'Viernes';
  static String satField = 'Sábado';
  static String sunField = 'Domingo';
  static String monValField = 'Lun, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String tueValField = 'Mar, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String wedValField = 'Mié, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String thuValField = 'Jue, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String friValField = 'Vie, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String satValField = 'Sáb, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String sunValField = 'Dom, ej: 8:00 am - 5:00 pm (dejar vacío si no se abre este día)';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = '¡Horario de atención editado!\n\n';
  static String successBody = 'Tu horario de atención se ha editado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'Tu horario de atención no se ha podido editar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class ContactFormTexts {
  static String webField = 'Web';
  static String emailField = 'Correo';
  static String facebookField = 'Facebook';
  static String instagramField = 'Instagram';
  static String twitterField = 'Twitter';
  static String youtubeField = 'Youtube';
  static String adoptionField = 'Link de adopción';
  static String webValField = 'Web, ej: https://www.dominio.com';
  static String emailValField = 'Correo, ej: nombre@dominio.com';
  static String facebookValField = 'Facebook, ej: https://www.facebook.com/NombrePagina';
  static String instagramValField = 'Instagram, ej: https://www.instagram.com/NombreCuenta';
  static String twitterValField = 'Twitter, ej: https://www.twitter.com/NombreCuenta';
  static String youtubeValField = 'Youtube, ej: https://www.youtube.com/NombreCanal';
  static String adoptionValField = 'Link de adopción (puede ser el mismo correo)';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = '¡Redes de contacto editadas!\n\n';
  static String successBody = 'Tus redes se han editado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'Tus redes no se han podido editar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class ProductTexts {
  static String title = 'Productos';
  static String noProductsFound = 'No hay productos registrados';
  static String tableColumnOne = 'Producto';
  static String tableColumnTwo = 'Acciones';

  static String editTitle = 'Editar Producto\n\n';
  static String editBody = '¿Está seguro que desea editar el registro?';
  static String deleteTitle = 'Eliminar Producto\n\n';
  static String deleteBody = '¿Está seguro que desea eliminar el registro?';

  static String btnCancel = 'Cancelar';
  static String btnEdit = 'Editar';
  static String btnDelete = 'Eliminar';
  static String btnShowDetails = 'Ver Detalles';
  static String btnClose = 'Cerrar';
}

class ProductFormTexts {
  static String newTitle = 'Nuevo Producto';
  static String editTitle = 'Editar Producto';

  static String nameField = 'Nombre';
  static String priceField = 'Precio';
  static String smallDescField = 'Descripción corta';
  static String fullDescField = 'Descripción completa';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';
  static String requiredPicture = 'Debes subir una foto de tu producto';

  static String successTitle = '¡Producto registrado!\n\n';
  static String successBody = 'Tu producto se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'Tu producto no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class PetTexts {
  static String title = 'Mascotas';
  static String noPetsFound = 'No hay mascotas registradas';
  static String tableColumnOne = 'Mascota';
  static String tableColumnTwo = 'Acciones';

  static String editTitle = 'Editar Mascota\n\n';
  static String editBody = '¿Está seguro que desea editar el registro?';
  static String deleteTitle = 'Eliminar Mascota\n\n';
  static String deleteBody = '¿Está seguro que desea eliminar el registro?';

  static String btnCancel = 'Cancelar';
  static String btnEdit = 'Editar';
  static String btnDelete = 'Eliminar';
  static String btnShowDetails = 'Ver Detalles';
  static String btnClose = 'Cerrar';
}

class PetFormTexts {
  static String newTitle = 'Nueva Mascota';
  static String editTitle = 'Editar Mascota';

  static String nameField = 'Nombre';
  static String ageField = 'Edad';
  static String descriptionField = 'Descripción corta';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';
  static String requiredPicture = 'Debes subir una foto de la mascota';

  static String successTitle = '¡Mascota registrada!\n\n';
  static String successBody = 'La mascota se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'La mascota no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class BankTexts {
  static String title = 'Bancos / Servicios';
  static String noBanksFound = 'No hay bancos o servicios registrados';
  static String tableColumnOne = 'Banco / Servicio';
  static String tableColumnTwo = 'Acciones';

  static String editTitle = 'Editar Banco\n\n';
  static String editBody = '¿Está seguro que desea editar el registro?';
  static String deleteTitle = 'Eliminar Banco\n\n';
  static String deleteBody = '¿Está seguro que desea eliminar el registro?\n\nSe eliminarán todas las cuentas asociadas a este banco / servicio.';

  static String btnCancel = 'Cancelar';
  static String btnEdit = 'Editar';
  static String btnDelete = 'Eliminar';
  static String btnShowAccounts = 'Ver Cuentas Asociadas';
}

class AccountTexts {
  static String title = 'Cuentas';
  static String noAccountsFound = 'No hay cuentas registradas';
  static String tableColumnOne = 'Cuenta';
  static String tableColumnTwo = 'Acciones';

  static String editTitle = 'Editar Cuenta\n\n';
  static String editBody = '¿Está seguro que desea editar el registro?';
  static String deleteTitle = 'Eliminar Cuenta\n\n';
  static String deleteBody = '¿Está seguro que desea eliminar el registro?';

  static String btnCancel = 'Cancelar';
  static String btnEdit = 'Editar';
  static String btnDelete = 'Eliminar';
}

class BankFormTexts {
  static String newTitle = 'Nuevo Banco / Servicio';
  static String editTitle = 'Editar Banco / Servicio';

  static String nameField = 'Nombre Banco / Servicio';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = '¡Banco / Servicio registrado!\n\n';
  static String successBody = 'El Banco / Servicio se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'El Banco / Servicio no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class AccountFormTexts {
  static String newTitle = 'Nueva Cuenta';
  static String editTitle = 'Editar Cuenta';

  static String currencyField = 'Moneda / Tipo de Servicio';
  static String accountNumberField = 'Número de cuenta / Correo / etc...';
  static String btnSave = 'Guardar';
  static String requiredField = 'Campo requerido';

  static String successTitle = 'Cuenta registrada!\n\n';
  static String successBody = 'La Cuenta se ha registrado exitosamente';
  static String errorTitle = '¡Ocurrió un problema!\n\n';
  static String errorBody = 'La Cuenta no se ha podido registrar, por favor verifica tu conexión e intenta de nuevo.';
  static String btnOk = 'Ok';
}

class ImagePickerTexts {
  static String gallery = 'Galería';
  static String camera = 'Cámara';

  static String deleteTitle = 'Eliminar Imagen\n\n';
  static String deleteBody = '¿Desea eliminar la imagen seleccionada?';
  static String btnCancel = 'Cancelar';
  static String btnDelete = 'Eliminar';
}

class CurrencyDropDownTexts {
  static String label = 'Moneda';
  static String defaultOpt = 'Seleccione una opción';
  static String cordoba = 'C\$';
  static String dollar = '\$';

  static String donationOpt1 = 'Córdobas C\$';
  static String donationOpt2 = 'Dólares \$';
  static String donationOpt3 = 'Correo';
  static String donationOpt4 = 'Billetera Móvil';
  static String donationOpt5 = 'Enlace';
}

class SexDropDownTexts {
  static String label = 'Género';
  static String defaultOpt = 'Seleccione una opción';
  static String male = 'Macho';
  static String female = 'Hembra';
}

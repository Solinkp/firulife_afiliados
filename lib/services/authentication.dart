import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<String?> signIn(String email, String password);

  Future<dynamic> getUserTypeClaim(User user);

  User? getCurrentUser();

  Future<void> signOut();

  Future<void> sendPasswordResetEmail(String email);
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String?> signIn(String email, String password) async {
    int? userType;
    String? message;

    UserCredential credential = await _firebaseAuth.signInWithEmailAndPassword(
      email: email, password: password
    );
    User? user = credential.user;
    if(user != null) {
      await getUserTypeClaim(user).then((claim) => userType = claim);
      if(userType == 0 || userType == 2) {
        signOut();
        message = 'Usuario no destinado para esta app.';
      }
    }
    return message;
  }

  Future<dynamic> getUserTypeClaim(User user) async {
    int? userType;
    await user.getIdTokenResult(true).then((idTokenResult) => {
      userType = idTokenResult.claims!['type'],
    });
    return userType;
  }

  User? getCurrentUser() {
    return _firebaseAuth.currentUser;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendPasswordResetEmail(String email) async {
    return _firebaseAuth.sendPasswordResetEmail(email: email);
  }

}

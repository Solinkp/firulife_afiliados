import 'dart:io';
import 'package:path/path.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:diacritic/diacritic.dart';

import '../models/business.dart';
import '../models/product.dart';
import '../models/pet.dart';
import './authentication.dart';

abstract class BaseDatabase {
  Stream<Business>? getLinkedBusiness();

  // INFO
  Future<bool> saveBusiness(Business business, bool isNew);

  Future<bool> deleteBusinessImage(int businessType, String pictureUrl);

  Future<String> uploadBusinessImage(
    int businessType,
    String businessName,
    File businessImage
  );

  // PRODUCTS
  Stream<List<Product>?> getBusinessProducts(String businessId);

  Future<bool> deleteProductPicture(int businessType, String pictureUrl);

  Future<String> uploadProductPicture(
    int businessType,
    String businessName,
    File productImage
  );

  Future<bool> saveProduct(Product product, bool isNew);

  Future<bool> deleteProduct(int businessType, String productId, String pictureUrl);

  // PETS
  Stream<List<Pet>?> getBusinessPets(String businessId);

  Future<bool> deletePetPicture(int businessType, String pictureUrl);

  Future<String> uploadPetPicture(
    int businessType,
    String businessName,
    File petImage
  );

  Future<bool> savePet(Pet pet, bool isNew);

  Future<bool> deletePet(int businessType, String petId, String pictureUrl);

}

class Database implements BaseDatabase {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Stream<Business>? getLinkedBusiness() {
    final BaseAuth auth = Auth();
    User? user = auth.getCurrentUser();
    if(user != null) {
      return _firestore.collection('business').where('userId', isEqualTo: user.uid).snapshots().map((snapshot) =>
        Business.buildBusiness(snapshot.docs.single.id, snapshot.docs.single.data())
      );
    } else {
      return null;
    }
  }

  // INFO
  Future<bool> saveBusiness(Business business, bool isNew) async {
    try {
      if(isNew) {
        await _firestore.collection('business').add(business.toMap());
      } else {
        await _firestore.collection('business').doc(business.id).update(business.toMap());
      }
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> deleteBusinessImage(int businessType, String pictureUrl) async {
    late String path;
    String pictureName;
    String businessFolder;
    switch(businessType) {
      case 0:
        businessFolder = pictureUrl.split("Shelters%2F")[1].split("%2F")[0];
        pictureName = pictureUrl.split("$businessFolder%2F")[1].split("?alt")[0];
        path = "Shelters/$businessFolder/$pictureName";
        break;
      case 1:
        businessFolder = pictureUrl.split("Vets%2F")[1].split("%2F")[0];
        pictureName = pictureUrl.split("$businessFolder%2F")[1].split("?alt")[0];
        path = "Vets/$businessFolder/$pictureName";
        break;
      case 2:
        businessFolder = pictureUrl.split("Stores%2F")[1].split("%2F")[0];
        pictureName = pictureUrl.split("$businessFolder%2F")[1].split("?alt")[0];
        path = "Stores/$businessFolder/$pictureName";
        break;
      case 3:
        businessFolder = pictureUrl.split("Walkers%2F")[1].split("%2F")[0];
        pictureName = pictureUrl.split("$businessFolder%2F")[1].split("?alt")[0];
        path = 'Walkers/$businessFolder/$pictureName';
        break;
      case 4:
        businessFolder = pictureUrl.split("Indep_Vets%2F")[1].split("%2F")[0];
        pictureName = pictureUrl.split("$businessFolder%2F")[1].split("?alt")[0];
        path = 'Indep_Vets/$businessFolder/$pictureName';
        break;
    }
    try {
      FirebaseStorage.instance.ref().child(path).delete();
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<String> uploadBusinessImage(int businessType, String businessName, File businessImage) async {
    String businessFolder = removeDiacritics(businessName.replaceAll(' ', '_'));
    String path = '';
    String fileName = businessFolder+'_Logo.png';
    switch(businessType) {
      case 0:
        path = "Shelters/$businessFolder/$fileName";
        break;
      case 1:
        path = "Vets/$businessFolder/$fileName";
        break;
      case 2:
        path = "Stores/$businessFolder/$fileName";
        break;
      case 3:
        path = 'Walkers/$businessFolder/$fileName';
        break;
      case 4:
        path = 'Indep_Vets/$businessFolder/$fileName';
        break;
    }
    Reference storageReference = FirebaseStorage.instance.ref().child(path);
    await storageReference.putFile(businessImage);
    String fileUrl = '';
    await storageReference.getDownloadURL().then((url) {
      fileUrl = url;
    });
    return fileUrl;
  }

  // PRODUCTS
  Stream<List<Product>?> getBusinessProducts(String businessId) {
    return _firestore.collection('product').where('businessId', isEqualTo: businessId).orderBy('name', descending: false).snapshots().map((snapshot) {
      return snapshot.docs.map((document) => Product.buildProduct(document.id, document)).toList();
    });
  }

  Future<bool> deleteProductPicture(int businessType, String pictureUrl) async {
    late String path;
    String businessFolder;
    String pictureName = pictureUrl.split("Products%2F")[1].split("?alt")[0];
    switch(businessType) {
      case 0:
        businessFolder = pictureUrl.split("Shelters%2F")[1].split("%2FProducts")[0];
        path = "Shelters/$businessFolder/Products/$pictureName";
        break;
      case 1:
        businessFolder = pictureUrl.split("Vets%2F")[1].split("%2FProducts")[0];
        path = "Vets/$businessFolder/Products/$pictureName";
        break;
      case 2:
        businessFolder = pictureUrl.split("Stores%2F")[1].split("%2FProducts")[0];
        path = "Stores/$businessFolder/Products/$pictureName";
        break;
      case 3:
        businessFolder = pictureUrl.split("Walkers%2F")[1].split("%2FProducts")[0];
        path = 'Walkers/$businessFolder/Products/$pictureName';
        break;
      case 4:
        businessFolder = pictureUrl.split("Indep_Vets%2F")[1].split("%2FProducts")[0];
        path = 'Indep_Vets/$businessFolder/Products/$pictureName';
        break;
    }
    try {
      FirebaseStorage.instance.ref().child(path).delete();
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<String> uploadProductPicture(int businessType, String businessName, File productImage) async {
    String fileName = basename(productImage.path);
    String path = '';
    String businessFolder = removeDiacritics(businessName.replaceAll(' ', '_'));
    switch(businessType) {
      case 0:
        path = "Shelters/$businessFolder/Products/$fileName";
        break;
      case 1:
        path = "Vets/$businessFolder/Products/$fileName";
        break;
      case 2:
        path = "Stores/$businessFolder/Products/$fileName";
        break;
      case 3:
        path = 'Walkers/$businessFolder/Products/$fileName';
        break;
      case 4:
        path = 'Indep_Vets/$businessFolder/Products/$fileName';
        break;
    }
    Reference storageReference = FirebaseStorage.instance.ref().child(path);
    await storageReference.putFile(productImage);
    String fileUrl = '';
    await storageReference.getDownloadURL().then((url) {
      fileUrl = url;
    });
    return fileUrl;
  }

  Future<bool> saveProduct(Product product, bool isNew) async {
    try {
      if(isNew) {
        await _firestore.collection('product').add(product.toMap());
      } else {
        await _firestore.collection('product').doc(product.id).update(product.toMap());
      }
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> deleteProduct(int businessType, String productId, String pictureUrl) async {
    try {
      _firestore.collection('product').doc(productId).delete();
      await deleteProductPicture(businessType, pictureUrl);
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  // PETS
  Stream<List<Pet>?> getBusinessPets(String businessId) {
    return _firestore.collection('pet').where('businessId', isEqualTo: businessId).orderBy('name', descending: false).snapshots().map((snapshot) {
      return snapshot.docs.map((document) => Pet.buildPet(document.id, document)).toList();
    });
  }

  Future<bool> deletePetPicture(int businessType, String pictureUrl) async {
    String pictureName = pictureUrl.split("Firus%2F")[1].split("?alt")[0];
    String businessFolder = pictureUrl.split("Shelters%2F")[1].split("%2FFirus")[0];
    String path = "Shelters/$businessFolder/Firus/$pictureName";

    try {
      FirebaseStorage.instance.ref().child(path).delete();
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<String> uploadPetPicture(int businessType, String businessName, File petImage) async {
    String fileName = basename(petImage.path);
    String businessFolder = removeDiacritics(businessName.replaceAll(' ', '_'));
    String path = "Shelters/$businessFolder/Firus/$fileName";

    Reference storageReference = FirebaseStorage.instance.ref().child(path);
    await storageReference.putFile(petImage);
    String fileUrl = '';
    await storageReference.getDownloadURL().then((url) {
      fileUrl = url;
    });
    return fileUrl;
  }

  Future<bool> savePet(Pet pet, bool isNew) async {
    try {
      if(isNew) {
        await _firestore.collection('pet').add(pet.toMap());
      } else {
        await _firestore.collection('pet').doc(pet.id).update(pet.toMap());
      }
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> deletePet(int businessType, String petId, String pictureUrl) async {
    try {
      _firestore.collection('pet').doc(petId).delete();
      await deletePetPicture(businessType, pictureUrl);
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'account_detail.freezed.dart';
part 'account_detail.g.dart';

@freezed
class AccountDetail with _$AccountDetail {
  const factory AccountDetail({
    String? holder,
    String? currency,
    String? number,
    String? link,
  }) = _AccountDetail;

  factory AccountDetail.fromJson(Map<String, dynamic> json) =>
      _$AccountDetailFromJson(json);
}

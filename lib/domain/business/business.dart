import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:firulife_afiliados/domain/business/account/account.dart';
import 'package:firulife_afiliados/domain/business/address/address.dart';
import 'package:firulife_afiliados/domain/business/contact/contact.dart';
import 'package:firulife_afiliados/domain/business/schedule/schedule.dart';

part 'business.freezed.dart';
part 'business.g.dart';

@freezed
class Business with _$Business {
  const factory Business({
    required String id,
    required int type,
    required String name,
    required List<Contact> contacts,
    required List<Schedule> schedule,
    String? picture,
    List<String>? phone,
    List<String>? services,
    List<Address>? addresses,
    List<Account>? donationAccounts,
  }) = _Business;

  factory Business.fromJson(Map<String, dynamic> json) =>
      _$BusinessFromJson(json);
}

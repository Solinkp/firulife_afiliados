import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:path_provider/path_provider.dart';

import 'package:firulife_afiliados/presentation/firu_main.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  // await _initHive();
  // await di.configure();

  runApp(const ProviderScope(
    child: FiruMain(),
  ));
}

// Future<void> _initHive() async {
  // final appDocumentDir = await getApplicationDocumentsDirectory();
  // await Hive.initFlutter(appDocumentDir.path);

  /// Register adapters
  // Hive.registerAdapter<UserHiveModel>(UserAdapter());

  /// Open boxes
  // await Hive.openBox(kAppBox);
  // await Hive.openBox<UserHiveModel>(kUserBox);
// }

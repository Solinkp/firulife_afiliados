import 'package:firulife_afiliados/domain/business/business.dart';

abstract class IBusinessDataSource {
  Stream<List<Business>> getBusinesses();

  Future<Stream<Business>> getBusiness(String businessId);
}

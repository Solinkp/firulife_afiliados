import 'package:firulife_afiliados/domain/business/business.dart';
import 'package:injectable/injectable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firulife_afiliados/infrastructure/business/data_source/i_business_data_source.dart';

@LazySingleton(as: BusinessDataSource)
class BusinessDataSource implements IBusinessDataSource {
  // final _firestore = locator<FirebaseFirestore>();

  @override
  Future<Stream<Business>> getBusiness(String businessId) async {
    final businessRef = _firestore.collection('business');
    final businessDoc = await businessRef.doc(businessId).get();
    print(businessDoc.id);
    print(businessDoc.data());
    throw '';
  }

  @override
  Stream<List<Business>> getBusinesses() {
    final businessRef = _firestore.collection('business');
    return businessRef
        .where('type', isEqualTo: 0)
        .where('active', isEqualTo: true)
        .snapshots()
        .map((querySnapshot) {
      return querySnapshot.docs.map((doc) {
        final businessJson = doc.data();
        businessJson.putIfAbsent('id', () => doc.id);
        return Business.fromJson(businessJson);
      }).toList();
    });
  }
}

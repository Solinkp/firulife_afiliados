import 'package:injectable/injectable.dart';
import 'package:firulife_afiliados/domain/business/business.dart';
import 'package:firulife_afiliados/infrastructure/business/repository/i_business_repository.dart';
import 'package:firulife_afiliados/infrastructure/business/data_source/i_business_data_source.dart';

@LazySingleton(as: BusinessRepository)
class BusinessRepository implements IBusinessRepository {
  final IBusinessDataSource _businessDataSource;

  const BusinessRepository(this._businessDataSource);

  @override
  Stream<List<Business>> getBusinesses() => _businessDataSource.getBusinesses();

  @override
  Future<Stream<Business>> getBusiness(String businessId) async =>
      await _businessDataSource.getBusiness(businessId);
}

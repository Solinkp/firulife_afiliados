import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../settings/firu_assets.dart';

class FiruLogo extends StatelessWidget {
  final Color color;
  final double radius;
  final String imageAsset;

  FiruLogo({
    this.color = Colors.transparent,
    this.radius = 70.0,
    this.imageAsset = FiruAssets.firuLogoWhite
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'hero_firu_logo',
      child: CircleAvatar(
        backgroundColor: color,
        radius: radius,
        child: SvgPicture.asset(imageAsset)
      )
    );
  }

}

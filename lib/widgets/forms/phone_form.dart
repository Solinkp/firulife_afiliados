import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../services/database.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show PhoneFormTexts;
import '../spinner_loader.dart';
import '../custom_button.dart';
import '../custom_text_button.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';

class PhoneForm extends StatefulWidget {
  final Business business;
  final int? itemIndex;

  PhoneForm(this.business, this.itemIndex);

  @override
  _PhoneFormState createState() => _PhoneFormState();
}

class _PhoneFormState extends State<PhoneForm> {
  final BaseDatabase _database = Database();
  final _formKey = GlobalKey<FormState>();
  List<String> _businessPhone = [];
  late TextEditingController _phoneCtrl;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _phoneCtrl = TextEditingController();
    _businessPhone = widget.business.phone.cast();
    if(widget.itemIndex != null) {
      _phoneCtrl.text = _businessPhone[widget.itemIndex!];
    }
    super.initState();
  }

  @override
  void dispose() {
    _phoneCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: PhoneFormTexts.phoneField,
              icon: Icons.phone,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _phoneCtrl,
              theme: false,
              last: true
            ),
            CustomButton(
              text: PhoneFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _phoneCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? PhoneFormTexts.requiredField : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _updateBusinessPhones();
      _database.saveBusiness(widget.business, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _updateBusinessPhones() {
    if(widget.itemIndex == null) {
      _businessPhone.add(_phoneCtrl.text);
    } else {
      _businessPhone[widget.itemIndex!] = _phoneCtrl.text;
    }
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: PhoneFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: PhoneFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PhoneFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: PhoneFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: PhoneFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PhoneFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

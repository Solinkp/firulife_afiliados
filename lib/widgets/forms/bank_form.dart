import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../services/database.dart';
import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show BankFormTexts;
import '../custom_text_button.dart';
import '../spinner_loader.dart';
import '../custom_button.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';

class BankForm extends StatefulWidget {
  final String? bankKey;

  BankForm(this.bankKey);

  @override
  _BankFormState createState() => _BankFormState();
}

class _BankFormState extends State<BankForm> {
  final BaseDatabase _database = Database();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Map _businessDonationAccounts = {};
  late TextEditingController _keyCtrl;
  Business? _business;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _keyCtrl = TextEditingController();
    if(widget.bankKey != null) {
      _keyCtrl.text = widget.bankKey!;
    }
    super.initState();
  }

  @override
  void dispose() {
    _keyCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _business = Provider.of<Business?>(context);

    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: BankFormTexts.nameField,
              icon: Icons.text_fields,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _keyCtrl,
              theme: false,
            ),
            CustomButton(
              text: BankFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _keyCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? BankFormTexts.requiredField : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      buildBusinessDonationAccounts();
      Business formBusiness = Business(
        active: _business!.active,
        id: _business!.id,
        userId: _business!.userId,
        addresses: _business!.addresses,
        type: _business!.type,
        businessColor: _business!.businessColor,
        countryId: _business!.countryId,
        name: _business!.name,
        pictureUrl: _business!.pictureUrl,
        openingHours: _business!.openingHours,
        contact: _business!.contact,
        adoptionLink: _business!.adoptionLink,
        phone: _business!.phone,
        services: _business!.services,
        // edit
        donationAccounts: _businessDonationAccounts.cast<String, List<Map<String, String>>>()
      );
      _database.saveBusiness(formBusiness, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildBusinessDonationAccounts() {
    Map donationAccounts = _business!.donationAccounts!.cast();

    if(widget.bankKey != null) {
      donationAccounts.forEach((key, value) {
        String newKey;
        if(key.toString() == widget.bankKey) {
          newKey = _keyCtrl.text;
        } else {
          newKey = key.toString();
        }
        _businessDonationAccounts.putIfAbsent(newKey, () => value);
      });
    } else {
      if(donationAccounts.length > 0) {
        donationAccounts.forEach((key, value) {
          _businessDonationAccounts.putIfAbsent(key, () => value);
        });
        _businessDonationAccounts.putIfAbsent(_keyCtrl.text, () => []);
      } else {
        _businessDonationAccounts.putIfAbsent(_keyCtrl.text, () => []);
      }
    }
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: BankFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: BankFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: BankFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: BankFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: BankFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: BankFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

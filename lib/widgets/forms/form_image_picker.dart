import 'package:flutter/material.dart';
import 'dart:io';

import '../../settings/custom_colors.dart';
import '../../settings/placeholder_images.dart';

class FormImagePicker extends StatelessWidget {
  final File? picture;
  final String pictureUrl;
  final Function() showPicturePicker;
  final Function()? removePicture;
  final int type;
  final double _imageSize = 140;

  FormImagePicker({
    required this.picture,
    required this.pictureUrl,
    required this.showPicturePicker,
    required this.removePicture,
    required this.type
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        height: _imageSize,
        width: _imageSize,
        child: InkWell(
          onTap: showPicturePicker,
          onLongPress: removePicture,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: CustomColors.mainDark)
            ),
            child: picture != null ? ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              child: Image.file(
                picture!,
                fit: BoxFit.contain
              )
            ) : pictureUrl.isNotEmpty ? ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              child: FadeInImage(
                fit: BoxFit.contain,
                placeholder: type == 0 ? const AssetImage(PlaceholderImages.placeholderBannerWithBack) : type == 1 ? const AssetImage(PlaceholderImages.placeholderProduct) : const AssetImage(PlaceholderImages.placeholderFiru),
                image: NetworkImage(pictureUrl),
                imageErrorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
                  return Image.asset(PlaceholderImages.placeholderProduct);
                }
              )
            ) : Icon(
              Icons.camera_alt,
              color: Colors.grey
            )
          )
        )
      )
    );
  }

}

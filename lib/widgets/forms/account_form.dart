import 'package:flutter/material.dart';

import '../../services/database.dart';
import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show AccountFormTexts, CurrencyDropDownTexts;
import '../custom_text_button.dart';
import '../spinner_loader.dart';
import '../custom_button.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';
import './form_currency_drop_down.dart';

class AccountForm extends StatefulWidget {
  final String bankKey;
  final Business business;
  final int? index;

  AccountForm(this.bankKey, this.business, this.index);

  @override
  _AccountFormState createState() => _AccountFormState();
}

class _AccountFormState extends State<AccountForm> {
  final BaseDatabase _database = Database();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late TextEditingController _currencyCtrl;
  late TextEditingController _accountNumberCtrl;
  String _errorMessage = '';
  bool _isLoading = false;
  Map _donationAccounts = {};
  List _accounts = [];
  Map _accountDetails = {};

  @override
  void initState() {
    _currencyCtrl = TextEditingController();
    _accountNumberCtrl = TextEditingController();
    _donationAccounts = widget.business.donationAccounts!.cast();
    _accounts = _donationAccounts[widget.bankKey];
    if(widget.index != null) {
      _accountDetails = _accounts[widget.index!];
      _currencyCtrl.text = _accountDetails.keys.single.toString();
      _accountNumberCtrl.text = _accountDetails.values.single.toString();
    }
    super.initState();
  }

  @override
  void dispose() {
    _currencyCtrl.dispose();
    _accountNumberCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            FormCurrencyDropDown(
              value: _currencyCtrl.text,
              label: AccountFormTexts.currencyField,
              getItems: [
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.defaultOpt), value: ''),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.donationOpt1), value: CurrencyDropDownTexts.donationOpt1),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.donationOpt2), value: CurrencyDropDownTexts.donationOpt2),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.donationOpt3), value: CurrencyDropDownTexts.donationOpt3),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.donationOpt4), value: CurrencyDropDownTexts.donationOpt4),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.donationOpt5), value: CurrencyDropDownTexts.donationOpt5),
              ],
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue,
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: AccountFormTexts.accountNumberField,
              icon: Icons.text_fields,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _accountNumberCtrl,
              theme: false,
              last: true
            ),
            CustomButton(
              text: AccountFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      _accountNumberCtrl.text = value.trim();
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    return value.toString().trim().isEmpty ? AccountFormTexts.requiredField : null;
  }

  String? _dropDownvalidatorFunction(dynamic value) {
    return (value == null || value.toString().isEmpty || value == CurrencyDropDownTexts.defaultOpt) ? AccountFormTexts.requiredField : null;
  }

  void _onChangedDropDownValue(dynamic value) {
    setState(() {
      _currencyCtrl.text = value;
    });
  }

  void _onSavedDropDownValue(dynamic value) {
    _currencyCtrl.text = value;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _updateBusinessDonationAccount();
      _database.saveBusiness(widget.business, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _updateBusinessDonationAccount() {
    Map newMap = {_currencyCtrl.text: _accountNumberCtrl.text};
    if(widget.index == null) {
      _accounts.add(newMap);
    } else {
      _accounts[widget.index!] = newMap;
    }
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: AccountFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: AccountFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: AccountFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: AccountFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: AccountFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: AccountFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

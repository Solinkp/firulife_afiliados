import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../../services/database.dart';
import '../../models/business.dart';
import '../../models/product.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show ProductFormTexts, CurrencyDropDownTexts, ImagePickerTexts;
import '../colored_safe_area.dart';
import '../custom_text_button.dart';
import '../spinner_loader.dart';
import '../custom_button.dart';
import './custom_text_form_field.dart';
import './form_image_picker.dart';
import './form_currency_drop_down.dart';
import './form_error_message.dart';

class ProductForm extends StatefulWidget {
  final Product? product;

  ProductForm({this.product});

  @override
  _ProductFormState createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {
  final BaseDatabase _database = Database();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late TextEditingController _nameCtrl;
  late TextEditingController _currencyCtrl;
  late TextEditingController _priceCtrl;
  late TextEditingController _smallDescriptionCtrl;
  late TextEditingController _fullDescriptionCtrl;
  Business? _business;
  File? _picture;
  String _pictureUrl = '';
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _nameCtrl = TextEditingController();
    _currencyCtrl = TextEditingController();
    _priceCtrl = TextEditingController();
    _smallDescriptionCtrl = TextEditingController();
    _fullDescriptionCtrl = TextEditingController();

    if(widget.product != null) {
      _nameCtrl.text = widget.product!.name;
      _currencyCtrl.text = widget.product!.currency;
      _priceCtrl.text = widget.product!.price.toString();
      _smallDescriptionCtrl.text = widget.product!.smallDescription;
      _fullDescriptionCtrl.text = widget.product!.fullDescription;
      _pictureUrl = widget.product!.pictureUrl;
    }
    super.initState();
  }

  @override
  void dispose() {
    _nameCtrl.dispose();
    _currencyCtrl.dispose();
    _priceCtrl.dispose();
    _smallDescriptionCtrl.dispose();
    _fullDescriptionCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _business = Provider.of<Business?>(context);

    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [FormImagePicker(
                picture: _picture,
                pictureUrl: _pictureUrl,
                showPicturePicker: _showPicturePicker,
                removePicture: _removePicture,
                type: 1
              )]
            ),
            // Name Input
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ProductFormTexts.nameField,
              icon: Icons.text_fields,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _nameCtrl,
              theme: false,
            ),
            // Currency and Price Inputs
            FormCurrencyDropDown(
              value: _currencyCtrl.text,
              label: CurrencyDropDownTexts.label,
              getItems: [
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.defaultOpt), value: ''),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.cordoba), value: CurrencyDropDownTexts.cordoba),
                DropdownMenuItem(child: Text(CurrencyDropDownTexts.dollar), value: CurrencyDropDownTexts.dollar)
              ],
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue,
            ),
            CustomTextFormField(
              inputType: TextInputType.numberWithOptions(decimal: true),
              isNumber: true,
              charLength: 7,
              hintText: ProductFormTexts.priceField,
              icon: Icons.money,
              index: 1,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _priceCtrl,
              theme: false
            ),
            // Descriptions Inputs
            CustomTextFormField(
              inputType: TextInputType.multiline,
              lines: 2,
              hintText: ProductFormTexts.smallDescField,
              icon: Icons.description,
              index: 2,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _smallDescriptionCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.multiline,
              lines: 3,
              hintText: ProductFormTexts.fullDescField,
              icon: Icons.description,
              index: 3,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _fullDescriptionCtrl,
              theme: false,
              last: true,
            ),
            CustomButton(
              text: ProductFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _showPicturePicker() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return ColoredSafeArea(
          child: Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: const Icon(Icons.photo_library, color: CustomColors.secondaryGreen),
                  title: Text(ImagePickerTexts.gallery, style: TextStyle(color: Colors.white)),
                  onTap: () {
                    _getImageFromSource(ImageSource.gallery);
                    Navigator.of(context).pop();
                  }
                ),
                ListTile(
                  leading: const Icon(Icons.camera_alt, color: CustomColors.secondaryGreen),
                  title: Text(ImagePickerTexts.camera, style: TextStyle(color: Colors.white)),
                  onTap: () {
                    _getImageFromSource(ImageSource.camera);
                    Navigator.of(context).pop();
                  }
                )
              ]
            )
          )
        );
      }
    );
  }

  void _getImageFromSource(ImageSource source) async {
    final ImagePicker picker = ImagePicker();
    PickedFile? image = await picker.getImage(
      source: source,
      imageQuality: 50
    );
    if(image != null) {
      setState(() {
        _picture = File(image.path);
      });
    }
  }

  void _removePicture() {
    if(_picture != null) {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: CustomColors.mainDark,
            content: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(fontSize: 16, color: Colors.white),
                children: <TextSpan>[
                  TextSpan(text: ImagePickerTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ImagePickerTexts.deleteBody)
                ]
              )
            ),
            actions: [
              CustomTextButton(
                text: ImagePickerTexts.btnCancel,
                pressCallback: () => Navigator.of(context).pop()
              ),
              CustomTextButton(
                text: ImagePickerTexts.btnDelete,
                pressCallback: () {
                  setState(() {
                    _picture = null;
                  });
                  Navigator.of(context).pop();
                }
              )
            ]
          );
        }
      );
    }
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _nameCtrl.text = value.trim();
      } else if(index == 1) {
        _priceCtrl.text = value.trim();
      } else if(index == 2) {
        _smallDescriptionCtrl.text = value.trim();
      } else if(index == 3) {
        _fullDescriptionCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? ProductFormTexts.requiredField : null;
    } else if(index == 1) {
      return value.toString().trim().isEmpty ? ProductFormTexts.requiredField : null;
    } else if(index == 2) {
      return value.toString().trim().isEmpty ? ProductFormTexts.requiredField : null;
    } else if(index == 3) {
      return value.toString().trim().isEmpty ? ProductFormTexts.requiredField : null;
    }
    return null;
  }

  String? _dropDownvalidatorFunction(dynamic value) {
    return (value == null || value.toString().isEmpty || value == CurrencyDropDownTexts.defaultOpt) ? ProductFormTexts.requiredField : null;
  }

  void _onChangedDropDownValue(dynamic value) {
    setState(() {
      _currencyCtrl.text = value;
    });
  }

  void _onSavedDropDownValue(dynamic value) {
    _currencyCtrl.text = value;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(_picture == null) {
      if(_pictureUrl.isEmpty) {
        setState(() {
          _errorMessage = ProductFormTexts.requiredPicture;
        });
        return false;
      }
    }
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      if(widget.product == null) {
        _database.uploadProductPicture(_business!.type, _business!.name, _picture!).then((result) {
          _pictureUrl = result;
          _createProduct();
        }).catchError((e) {print('file $e');});
      } else {
        if(_picture != null) {
          _database.deleteProductPicture(_business!.type, _pictureUrl).then((result) {
            _database.uploadProductPicture(_business!.type, _business!.name, _picture!).then((result) {
              _pictureUrl = result;
              _editProduct();
            }).catchError((e) {print('file $e');});
          }).catchError((e) {print('delete picture $e');});
        } else {
          _editProduct();
        }
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _createProduct() async {
    Product newProduct = Product(
      businessId: _business!.id!,
      name: _nameCtrl.text,
      currency: _currencyCtrl.text,
      price: double.parse(_priceCtrl.text),
      smallDescription: _smallDescriptionCtrl.text,
      fullDescription: _fullDescriptionCtrl.text,
      pictureUrl: _pictureUrl
    );
    _database.saveProduct(newProduct, true).then((result) {
      setState(() {
        _isLoading = false;
      });
      if(result) {
        _showSuccess();
      } else {
        _showError();
      }
    }).catchError((e) {print('product = $e');});
  }

  void _editProduct() async {
    Product editedProduct = Product(
      id: widget.product!.id,
      businessId: widget.product!.businessId,
      name: _nameCtrl.text,
      currency: _currencyCtrl.text,
      price: double.parse(_priceCtrl.text),
      smallDescription: _smallDescriptionCtrl.text,
      fullDescription: _fullDescriptionCtrl.text,
      pictureUrl: _pictureUrl
    );
    _database.saveProduct(editedProduct, false).then((result) {
      setState(() {
        _isLoading = false;
      });
      if(result) {
        _showSuccess();
      } else {
        _showError();
      }
    }).catchError((e) {print('product = $e');});
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ProductFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ProductFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ProductFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ProductFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ProductFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ProductFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

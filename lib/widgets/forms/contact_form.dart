import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../services/database.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show ContactFormTexts;
import '../custom_button.dart';
import '../custom_text_button.dart';
import '../spinner_loader.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';

class ContactForm extends StatefulWidget {
  final Business business;

  ContactForm(this.business);

  @override
  _ContactFormState createState() => _ContactFormState();
}

class _ContactFormState extends State<ContactForm> {
  final BaseDatabase _database = Database();
  final _formKey = GlobalKey<FormState>();
  List<Map<String, String>> _contact = [];
  late TextEditingController _webCtrl;
  late TextEditingController _emailCtrl;
  late TextEditingController _facebookCtrl;
  late TextEditingController _instagramCtrl;
  late TextEditingController _twitterCtrl;
  late TextEditingController _youtubeCtrl;
  late TextEditingController _adoptionCtrl;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _webCtrl = TextEditingController();
    _emailCtrl = TextEditingController();
    _facebookCtrl = TextEditingController();
    _instagramCtrl = TextEditingController();
    _twitterCtrl = TextEditingController();
    _youtubeCtrl = TextEditingController();
    _adoptionCtrl = TextEditingController();
    _contact = widget.business.contact.cast<Map<String, String>>();
    List initContact = _contact.cast();
    if(initContact.isNotEmpty) {
      _webCtrl.text = initContact[0]['web'] == null ? '' : initContact[0]['web'];
      _emailCtrl.text = initContact[0]['email'] == null ? '' : initContact[0]['email'];
      _facebookCtrl.text = initContact[0]['facebook'] == null ? '' : initContact[0]['facebook'];
      _instagramCtrl.text = initContact[0]['instagram'] == null ? '' : initContact[0]['instagram'];
      _twitterCtrl.text = initContact[0]['twitter'] == null ? '' : initContact[0]['twitter'];
      _youtubeCtrl.text = initContact[0]['youtube'] == null ? '' : initContact[0]['youtube'];
    }
    _adoptionCtrl.text = widget.business.adoptionLink == null ? '' : widget.business.adoptionLink!;
    super.initState();
  }

  @override
  void dispose() {
    _webCtrl.dispose();
    _emailCtrl.dispose();
    _facebookCtrl.dispose();
    _instagramCtrl.dispose();
    _twitterCtrl.dispose();
    _youtubeCtrl.dispose();
    _adoptionCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            CustomTextFormField(
              inputType: TextInputType.url,
              hintText: ContactFormTexts.webField,
              helperText: ContactFormTexts.webValField,
              icon: Icons.text_fields,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _webCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.emailAddress,
              hintText: ContactFormTexts.emailField,
              helperText: ContactFormTexts.emailValField,
              icon: Icons.text_fields,
              index: 1,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _emailCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.url,
              hintText: ContactFormTexts.facebookField,
              helperText: ContactFormTexts.facebookValField,
              icon: Icons.text_fields,
              index: 2,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _facebookCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.url,
              hintText: ContactFormTexts.instagramField,
              helperText: ContactFormTexts.instagramValField,
              icon: Icons.text_fields,
              index: 3,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _instagramCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.url,
              hintText: ContactFormTexts.twitterField,
              helperText: ContactFormTexts.twitterValField,
              icon: Icons.text_fields,
              index: 4,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _twitterCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.url,
              hintText: ContactFormTexts.youtubeField,
              helperText: ContactFormTexts.youtubeValField,
              icon: Icons.text_fields,
              index: 5,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _youtubeCtrl,
              theme: false,
            ),
            widget.business.type == 0 ? CustomTextFormField(
              inputType: TextInputType.url,
              hintText: ContactFormTexts.adoptionField,
              helperText: ContactFormTexts.adoptionValField,
              icon: Icons.text_fields,
              index: 6,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _adoptionCtrl,
              theme: false,
              last: true,
            ) : Container(),
            CustomButton(
              text: ContactFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _webCtrl.text = value.trim();
      } else if(index == 1) {
        _emailCtrl.text = value.trim();
      } else if(index == 2) {
        _facebookCtrl.text = value.trim();
      } else if(index == 3) {
        _instagramCtrl.text = value.trim();
      } else if(index == 4) {
        _twitterCtrl.text = value.trim();
      } else if(index == 5) {
        _youtubeCtrl.text = value.trim();
      } else if(index == 6) {
        _adoptionCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 1) {
      return value.toString().trim().isEmpty ? ContactFormTexts.requiredField : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _editContact();
      _database.saveBusiness(widget.business, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _editContact() {
    List<Map<String, String>> newContact = [];
    Map<String, String> newItems = {};
    if(_webCtrl.text.isNotEmpty) {
      newItems.putIfAbsent('web', () => _webCtrl.text);
    }
    if(_emailCtrl.text.isNotEmpty) {
      newItems.putIfAbsent('email', () => _emailCtrl.text);
    }
    if(_facebookCtrl.text.isNotEmpty) {
      newItems.putIfAbsent('facebook', () => _facebookCtrl.text);
    }
    if(_instagramCtrl.text.isNotEmpty) {
      newItems.putIfAbsent('instagram', () => _instagramCtrl.text);
    }
    if(_twitterCtrl.text.isNotEmpty) {
      newItems.putIfAbsent('twitter', () => _twitterCtrl.text);
    }
    if(_youtubeCtrl.text.isNotEmpty) {
      newItems.putIfAbsent('youtube', () => _youtubeCtrl.text);
    }
    newContact.add(newItems);
    _contact.clear();
    _contact.addAll(newContact);
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ContactFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ContactFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ContactFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ContactFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ContactFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ContactFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

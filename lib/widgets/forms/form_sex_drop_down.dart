import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show SexDropDownTexts;

class FormSexDropDown extends StatefulWidget {
  final bool value;
  final Function(dynamic) validatorFunction;
  final Function(bool) setOnChangedValueForm;
  final Function(bool) setOnSavedValueForm;

  FormSexDropDown({
    required this.value,
    required this.validatorFunction,
    required this.setOnChangedValueForm,
    required this.setOnSavedValueForm
  });

  @override
  _FormSexDropDownState createState() => _FormSexDropDownState();
}

class _FormSexDropDownState extends State<FormSexDropDown> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      value: widget.value,
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: CustomColors.mainGreen)
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: CustomColors.mainDark)
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red)
        ),
        focusedErrorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red)
        ),
        labelText: SexDropDownTexts.label,
        labelStyle: TextStyle(color: CustomColors.mainDark),
        errorStyle: TextStyle(color: Colors.red),
        icon: Icon(
          MdiIcons.genderMaleFemale,
          color: CustomColors.mainDark
        )
      ),
      dropdownColor: CustomColors.secondaryGreen,
      iconEnabledColor: CustomColors.mainDark,
      iconDisabledColor: Colors.grey,
      style: TextStyle(color: CustomColors.mainDark),
      items: [
        DropdownMenuItem(child: Text(SexDropDownTexts.male), value: true),
        DropdownMenuItem(child: Text(SexDropDownTexts.female), value: false)
      ],
      validator: (value) => widget.validatorFunction(value),
      onChanged: (value) => widget.setOnChangedValueForm(value as bool),
      onSaved: (value) => widget.setOnSavedValueForm(value as bool)
    );
  }

}

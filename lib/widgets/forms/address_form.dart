import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../services/database.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show AddresFormTexts;
import '../spinner_loader.dart';
import '../custom_button.dart';
import '../custom_text_button.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';

class AddressForm extends StatefulWidget {
  final Business business;
  final int? itemIndex;

  AddressForm(this.business, this.itemIndex);

  @override
  _AddressFormState createState() => _AddressFormState();
}

class _AddressFormState extends State<AddressForm> {
  final BaseDatabase _database = Database();
  final _formKey = GlobalKey<FormState>();
  List<Map<String, String>> _businessAddress = [];
  late TextEditingController _addressCtrl;
  late TextEditingController _coordinatesCtrl;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _addressCtrl = TextEditingController();
    _coordinatesCtrl = TextEditingController();
    _businessAddress = widget.business.addresses;
    if(widget.itemIndex != null) {
      List address = _businessAddress.cast();
      _addressCtrl.text = address[widget.itemIndex!]['address'];
      _coordinatesCtrl.text = address[widget.itemIndex!]['coordinates'] == null ? '' : address[widget.itemIndex!]['coordinates'];
    }
    super.initState();
  }

  @override
  void dispose() {
    _addressCtrl.dispose();
    _coordinatesCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            CustomTextFormField(
              inputType: TextInputType.text,
              lines: 3,
              hintText: AddresFormTexts.addressField,
              icon: Icons.map_outlined,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _addressCtrl,
              theme: false,
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: AddresFormTexts.coordinatesField,
              icon: Icons.location_on,
              index: 1,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _coordinatesCtrl,
              theme: false,
              last: true
            ),
            CustomButton(
              text: AddresFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _addressCtrl.text = value.trim();
      } else if(index == 1) {
        _coordinatesCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? AddresFormTexts.requiredField : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _updateBusinessAddresses();
      _database.saveBusiness(widget.business, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _updateBusinessAddresses() {
    Map<String, String> newMap = {
      'address': _addressCtrl.text,
      'coordinates': _coordinatesCtrl.text
    };
    if(widget.itemIndex == null) {
      _businessAddress.add(newMap);
    } else {
      _businessAddress[widget.itemIndex!] = newMap;
    }
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: AddresFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: AddresFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: AddresFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: AddresFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: AddresFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: AddresFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

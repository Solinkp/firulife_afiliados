import 'package:flutter/material.dart';

class FormErrorMessage extends StatelessWidget {
  final String errorMessage;
  final double topPadding;
  final Color color;

  FormErrorMessage({
    required this.errorMessage,
    this.topPadding = 15,
    this.color = Colors.limeAccent
  });

  @override
  Widget build(BuildContext context) {
    if(errorMessage.length > 0) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: topPadding),
        child: Text(
          errorMessage,
          style: TextStyle(
            fontSize: 14.0,
            color: color,
            height: 1.0,
            fontWeight: FontWeight.w300
          )
        )
      );
    } else {
      return Container(height: 0.0, width: 0.0);
    }
  }

}

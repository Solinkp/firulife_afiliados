import 'package:flutter/material.dart';

import '../../settings/custom_colors.dart';

class FormCurrencyDropDown extends StatefulWidget {
  final String? value;
  final String label;
  final List<DropdownMenuItem<String>> getItems;
  final Function(dynamic) validatorFunction;
  final Function(String) setOnChangedValueForm;
  final Function(String) setOnSavedValueForm;

  FormCurrencyDropDown({
    required this.value,
    required this.label,
    required this.getItems,
    required this.validatorFunction,
    required this.setOnChangedValueForm,
    required this.setOnSavedValueForm
  });

  @override
  _FormCurrencyDropDownState createState() => _FormCurrencyDropDownState();
}

class _FormCurrencyDropDownState extends State<FormCurrencyDropDown> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      value: widget.value,
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: CustomColors.mainGreen)
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: CustomColors.mainDark)
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red)
        ),
        focusedErrorBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red)
        ),
        labelText: widget.label,
        labelStyle: TextStyle(color: CustomColors.mainDark),
        errorStyle: TextStyle(color: Colors.red),
        icon: Icon(
          Icons.attach_money,
          color: CustomColors.mainDark
        )
      ),
      dropdownColor: CustomColors.secondaryGreen,
      iconEnabledColor: CustomColors.mainDark,
      iconDisabledColor: Colors.grey,
      style: TextStyle(color: CustomColors.mainDark),
      items: widget.getItems,
      validator: (value) => widget.validatorFunction(value),
      onChanged: (value) => widget.setOnChangedValueForm(value.toString()),
      onSaved: (value) => widget.setOnSavedValueForm(value.toString())
    );
  }

}

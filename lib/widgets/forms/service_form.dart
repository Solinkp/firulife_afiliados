import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../services/database.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show ServiceFormTexts;
import '../spinner_loader.dart';
import '../custom_button.dart';
import '../custom_text_button.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';

class ServiceForm extends StatefulWidget {
  final Business business;
  final int? itemIndex;

  ServiceForm(this.business, this.itemIndex);

  @override
  _ServiceFormState createState() => _ServiceFormState();
}

class _ServiceFormState extends State<ServiceForm> {
  final BaseDatabase _database = Database();
  final _formKey = GlobalKey<FormState>();
  List<String> _businessService = [];
  late TextEditingController _serviceCtrl;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _serviceCtrl = TextEditingController();
    _businessService = widget.business.services.cast();
    if(widget.itemIndex != null) {
      _serviceCtrl.text = _businessService[widget.itemIndex!];
    }
    super.initState();
  }

  @override
  void dispose() {
    _serviceCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ServiceFormTexts.serviceField,
              icon: Icons.text_fields,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _serviceCtrl,
              theme: false,
              last: true
            ),
            CustomButton(
              text: ServiceFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _serviceCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? ServiceFormTexts.requiredField : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _updateBusinessServices();
      _database.saveBusiness(widget.business, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _updateBusinessServices() {
    if(widget.itemIndex == null) {
      _businessService.add(_serviceCtrl.text);
    } else {
      _businessService[widget.itemIndex!] = _serviceCtrl.text;
    }
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ServiceFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ServiceFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ServiceFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ServiceFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ServiceFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ServiceFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

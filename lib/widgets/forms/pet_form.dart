import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../../services/database.dart';
import '../../models/business.dart';
import '../../models/pet.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show PetFormTexts, ImagePickerTexts;
import '../colored_safe_area.dart';
import '../custom_text_button.dart';
import '../spinner_loader.dart';
import '../custom_button.dart';
import './custom_text_form_field.dart';
import './form_image_picker.dart';
import './form_sex_drop_down.dart';
import './form_error_message.dart';

class PetForm extends StatefulWidget {
  final Pet? pet;

  PetForm({this.pet});

  @override
  _PetFormState createState() => _PetFormState();
}

class _PetFormState extends State<PetForm> {
  final BaseDatabase _database = Database();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late TextEditingController _nameCtrl;
  late TextEditingController _ageCtrl;
  late TextEditingController _descriptionCtrl;
  bool _sex = true;
  Business? _business;
  File? _picture;
  String _pictureUrl = '';
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _nameCtrl = TextEditingController();
    _ageCtrl = TextEditingController();
    _descriptionCtrl = TextEditingController();

    if(widget.pet != null) {
      _nameCtrl.text = widget.pet!.name;
      _ageCtrl.text = widget.pet!.age.toString();
      _descriptionCtrl.text = widget.pet!.description;
      _sex = widget.pet!.sex;
      _pictureUrl = widget.pet!.pictureUrl;
    }
    super.initState();
  }

  @override
  void dispose() {
    _nameCtrl.dispose();
    _ageCtrl.dispose();
    _descriptionCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _business = Provider.of<Business?>(context);

    return Stack(
      children: <Widget>[
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [FormImagePicker(
                picture: _picture,
                pictureUrl: _pictureUrl,
                showPicturePicker: _showPicturePicker,
                removePicture: _removePicture,
                type: 2
              )]
            ),
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: PetFormTexts.nameField,
              icon: Icons.text_fields,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _nameCtrl,
              theme: false,
            ),
            FormSexDropDown(
              value: _sex,
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue,
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: PetFormTexts.ageField,
              icon: Icons.calendar_today,
              index: 1,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _ageCtrl,
              theme: false,
              isReadOnly: true,
              onTap: _pickDate,
            ),
            CustomTextFormField(
              inputType: TextInputType.multiline,
              lines: 2,
              hintText: PetFormTexts.descriptionField,
              icon: Icons.description,
              index: 2,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _descriptionCtrl,
              theme: false,
              last: true,
            ),
            CustomButton(
              text: PetFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _showPicturePicker() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return ColoredSafeArea(
          child: Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: const Icon(Icons.photo_library, color: CustomColors.secondaryGreen),
                  title: Text(ImagePickerTexts.gallery, style: TextStyle(color: Colors.white)),
                  onTap: () {
                    _getImageFromSource(ImageSource.gallery);
                    Navigator.of(context).pop();
                  }
                ),
                ListTile(
                  leading: const Icon(Icons.camera_alt, color: CustomColors.secondaryGreen),
                  title: Text(ImagePickerTexts.camera, style: TextStyle(color: Colors.white)),
                  onTap: () {
                    _getImageFromSource(ImageSource.camera);
                    Navigator.of(context).pop();
                  }
                )
              ]
            )
          )
        );
      }
    );
  }

  void _getImageFromSource(ImageSource source) async {
    final ImagePicker picker = ImagePicker();
    PickedFile? image = await picker.getImage(
      source: source,
      imageQuality: 50
    );
    if(image != null) {
      setState(() {
        _picture = File(image.path);
      });
    }
  }

  void _removePicture() {
    if(_picture != null) {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: CustomColors.mainDark,
            content: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(fontSize: 16, color: Colors.white),
                children: <TextSpan>[
                  TextSpan(text: ImagePickerTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ImagePickerTexts.deleteBody)
                ]
              )
            ),
            actions: [
              CustomTextButton(
                text: ImagePickerTexts.btnCancel,
                pressCallback: () => Navigator.of(context).pop()
              ),
              CustomTextButton(
                text: ImagePickerTexts.btnDelete,
                pressCallback: () {
                  setState(() {
                    _picture = null;
                  });
                  Navigator.of(context).pop();
                }
              )
            ]
          );
        }
      );
    }
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _nameCtrl.text = value.trim();
      } else if(index == 1) {
        _ageCtrl.text = value.trim();
      } else if(index == 2) {
        _descriptionCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? PetFormTexts.requiredField : null;
    } else if(index == 1) {
      return value.toString().trim().isEmpty ? PetFormTexts.requiredField : null;
    } else if(index == 2) {
      return value.toString().trim().isEmpty ? PetFormTexts.requiredField : null;
    }
    return null;
  }

  String? _dropDownvalidatorFunction(dynamic value) {
    return value == null ? PetFormTexts.requiredField : null;
  }

  void _onChangedDropDownValue(bool value) {
    setState(() {
      _sex = value;
    });
  }

  void _onSavedDropDownValue(bool value) {
    _sex = value;
  }

  Future<void> _pickDate() async {
    final int firstDate = DateTime.now().year - 12;
    final DateTime? date = await showDatePicker(
      context: context,
      initialDate: _ageCtrl.text.isEmpty ? DateTime.now() : DateTime(DateTime.parse(_ageCtrl.text).year, DateTime.parse(_ageCtrl.text).month, DateTime.parse(_ageCtrl.text).day),
      firstDate: DateTime(firstDate),
      lastDate: DateTime.now(),
      builder: (_, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: CustomColors.mainGreen
            )
          ),
          child: child!
        );
      }
    );
    if(date != null) {
      String finalDate = date.toString().split(" ")[0];
      setState(() {
        _ageCtrl.text = finalDate;
      });
    }
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(_picture == null) {
      if(_pictureUrl.isEmpty) {
        setState(() {
          _errorMessage = PetFormTexts.requiredPicture;
        });
        return false;
      }
    }
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      if(widget.pet == null) {
        _database.uploadPetPicture(_business!.type, _business!.name, _picture!).then((result) {
          _pictureUrl = result;
          _createPet();
        }).catchError((e) {print('file $e');});
      } else {
        if(_picture != null) {
          _database.deletePetPicture(_business!.type, _pictureUrl).then((result) {
            _database.uploadPetPicture(_business!.type, _business!.name, _picture!).then((result) {
              _pictureUrl = result;
              _editPet();
            }).catchError((e) {print('file $e');});
          }).catchError((e) {print('delete picture $e');});
        } else {
          _editPet();
        }
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _createPet() async {
    Pet newPet = Pet(
      businessId: _business!.id!,
      name: _nameCtrl.text,
      sex: _sex,
      age: _ageCtrl.text,
      description: _descriptionCtrl.text,
      pictureUrl: _pictureUrl
    );
    _database.savePet(newPet, true).then((result) {
      setState(() {
        _isLoading = false;
      });
      if(result) {
        _showSuccess();
      } else {
        _showError();
      }
    }).catchError((e) {print('pet = $e');});
  }

  void _editPet() async {
    Pet editedPet = Pet(
      id: widget.pet!.id,
      businessId: widget.pet!.businessId,
      name: _nameCtrl.text,
      sex: _sex,
      age: _ageCtrl.text,
      description: _descriptionCtrl.text,
      pictureUrl: _pictureUrl
    );
    _database.savePet(editedPet, false).then((result) {
      setState(() {
        _isLoading = false;
      });
      if(result) {
        _showSuccess();
      } else {
        _showError();
      }
    }).catchError((e) {print('pet = $e');});
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: PetFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: PetFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PetFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: PetFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: PetFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: PetFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../services/database.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show ScheduleFormTexts;
import '../custom_button.dart';
import '../custom_text_button.dart';
import '../spinner_loader.dart';
import './custom_text_form_field.dart';
import './form_error_message.dart';

class ScheduleForm extends StatefulWidget {
  final Business business;

  ScheduleForm(this.business);

  @override
  _ScheduleFormState createState() => _ScheduleFormState();
}

class _ScheduleFormState extends State<ScheduleForm> {
  final BaseDatabase _database = Database();
  final _formKey = GlobalKey<FormState>();
  List<Map<String, String>> _openingHours = [];
  late TextEditingController _monCtrl;
  late TextEditingController _tueCtrl;
  late TextEditingController _wedCtrl;
  late TextEditingController _thuCtrl;
  late TextEditingController _friCtrl;
  late TextEditingController _satCtrl;
  late TextEditingController _sunCtrl;
  String _errorMessage = '';
  bool _isLoading = false;

  @override
  void initState() {
    _monCtrl = TextEditingController();
    _tueCtrl = TextEditingController();
    _wedCtrl = TextEditingController();
    _thuCtrl = TextEditingController();
    _friCtrl = TextEditingController();
    _satCtrl = TextEditingController();
    _sunCtrl = TextEditingController();
    _openingHours = widget.business.openingHours.cast<Map<String, String>>();
    List initOpeningHours = _openingHours.cast();
    if(initOpeningHours.isNotEmpty) {
      _monCtrl.text = initOpeningHours[0]['schedule'];
      _tueCtrl.text = initOpeningHours[1]['schedule'];
      _wedCtrl.text = initOpeningHours[2]['schedule'];
      _thuCtrl.text = initOpeningHours[3]['schedule'];
      _friCtrl.text = initOpeningHours[4]['schedule'];
      _satCtrl.text = initOpeningHours[5]['schedule'];
      _sunCtrl.text = initOpeningHours[6]['schedule'];
    }
    super.initState();
  }

  @override
  void dispose() {
    _monCtrl.dispose();
    _tueCtrl.dispose();
    _wedCtrl.dispose();
    _thuCtrl.dispose();
    _friCtrl.dispose();
    _satCtrl.dispose();
    _sunCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _showForm(),
        _showLoader()
      ]
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return SpinnerLoader(color: CustomColors.mainGreen);
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.monField,
              helperText: ScheduleFormTexts.monValField,
              icon: Icons.access_time,
              index: 0,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _monCtrl,
              theme: false
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.tueField,
              helperText: ScheduleFormTexts.tueValField,
              icon: Icons.access_time,
              index: 1,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _tueCtrl,
              theme: false
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.wedField,
              helperText: ScheduleFormTexts.wedValField,
              icon: Icons.access_time,
              index: 2,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _wedCtrl,
              theme: false
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.thuField,
              helperText: ScheduleFormTexts.thuValField,
              icon: Icons.access_time,
              index: 3,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _thuCtrl,
              theme: false
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.friField,
              helperText: ScheduleFormTexts.friValField,
              icon: Icons.access_time,
              index: 4,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _friCtrl,
              theme: false
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.satField,
              helperText: ScheduleFormTexts.satValField,
              icon: Icons.access_time,
              index: 5,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _satCtrl,
              theme: false
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              hintText: ScheduleFormTexts.sunField,
              helperText: ScheduleFormTexts.sunValField,
              icon: Icons.access_time,
              index: 6,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              textController: _sunCtrl,
              theme: false,
              last: true
            ),
            CustomButton(
              text: ScheduleFormTexts.btnSave,
              pressCallback: _validateAndSubmit
            ),
            FormErrorMessage(errorMessage: _errorMessage, color: Colors.red)
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        value.isEmpty ? _monCtrl.text = 'CERRADO' : _monCtrl.text = value.trim();
      } else if(index == 1) {
        value.isEmpty ? _tueCtrl.text = 'CERRADO' : _tueCtrl.text = value.trim();
      } else if(index == 2) {
        value.isEmpty ? _wedCtrl.text = 'CERRADO' : _wedCtrl.text = value.trim();
      } else if(index == 3) {
        value.isEmpty ? _thuCtrl.text = 'CERRADO' : _thuCtrl.text = value.trim();
      } else if(index == 4) {
        value.isEmpty ? _friCtrl.text = 'CERRADO' : _friCtrl.text = value.trim();
      } else if(index == 5) {
        value.isEmpty ? _satCtrl.text = 'CERRADO' : _satCtrl.text = value.trim();
      } else if(index == 6) {
        value.isEmpty ? _sunCtrl.text = 'CERRADO' : _sunCtrl.text = value.trim();
      }
    });
  }

  String? _validatorFunction(int index, dynamic value) {
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState!;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _editSchedule();
      _database.saveBusiness(widget.business, false).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result) {
          _showSuccess();
        } else {
          _showError();
        }
      }).catchError((e) {print('business = $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _editSchedule() {
    List<Map<String, String>> newOpeningHours = [
      {
        'day': 'Lunes',
        'schedule': _monCtrl.text
      },
      {
        'day': 'Martes',
        'schedule': _tueCtrl.text
      },
      {
        'day': 'Miércoles',
        'schedule': _wedCtrl.text
      },
      {
        'day': 'Jueves',
        'schedule': _thuCtrl.text
      },
      {
        'day': 'Viernes',
        'schedule': _friCtrl.text
      },
      {
        'day': 'Sábado',
        'schedule': _satCtrl.text
      },
      {
        'day': 'Domingo',
        'schedule': _sunCtrl.text
      }
    ];
    _openingHours.clear();
    _openingHours.addAll(newOpeningHours);
  }

  void _showSuccess() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ScheduleFormTexts.successTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ScheduleFormTexts.successBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ScheduleFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showError() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: ScheduleFormTexts.errorTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ScheduleFormTexts.errorBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: ScheduleFormTexts.btnOk,
              pressCallback: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}

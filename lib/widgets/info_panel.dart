import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../routing/fade_route.dart';
import '../services/database.dart';
import '../models/business.dart';
import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show InfoPanelTexts;
import '../screens/business/forms/info_list_form_screen.dart';
import '../screens/business/forms/segment_form_screen.dart';
import './custom_text_button.dart';

class InfoPanel extends StatefulWidget {
  final String title;
  final int panelIndex;

  InfoPanel(this.title, this.panelIndex);

  @override
  _InfoPanelState createState() => _InfoPanelState();
}

class _InfoPanelState extends State<InfoPanel> {
  final BaseDatabase _database = Database();
  Business? _business;

  @override
  Widget build(BuildContext context) {
    _business = Provider.of<Business?>(context);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
      child: ExpansionTile(
        title: Center(child: Text(widget.title, style: Theme.of(context).textTheme.bodyText1)),
        children: _getChildren()
      )
    );
  }

  List<Widget> _getChildren() {
    List<Widget> children = [];
    List<Widget> childrenValue = [];

    switch (widget.panelIndex) {
      case 0:
        if(_business!.addresses.length > 0) {
          List addresses = _business!.addresses.cast();
          for(int i = 0; i < addresses.length; i++) {
            children.add(
              Container(child: Row(children: [
                _contentExpanded(addresses[i]['address'], Icons.location_on),
                _actionsExpanded(InfoPanelTexts.tooltipEditAdd, InfoPanelTexts.tooltipDeleteAdd, addresses, i)
              ]))
            );
          }
        }
        children.add(_segmentActionIcon(InfoPanelTexts.newAddress, true));
        break;
      case 1:
        if(_business!.phone.length > 0) {
          List phones = _business!.phone.cast();
          for(int i = 0; i < phones.length; i++) {
            children.add(
              Container(child: Row(children: [
                _contentExpanded(phones[i], Icons.phone),
                _actionsExpanded(InfoPanelTexts.tooltipEditPhone, InfoPanelTexts.tooltipDeletePhone, phones, i)
              ]))
            );
          }
        }
        children.add(_segmentActionIcon(InfoPanelTexts.newPhone, true));
        break;
      case 2:
        if(_business!.openingHours.length > 0) {
          List hours = _business!.openingHours.cast();
          for(int i = 0; i < hours.length; i++) {
            children.add(
              Row(children: [
                Expanded(
                  flex: 2,
                  child: Text('${hours[i]['day']}:', textAlign: TextAlign.center, style: Theme.of(context).textTheme.bodyText1)
                ),
                Expanded(
                  flex: 5,
                  child: ListTile(
                    title: Text(hours[i]['schedule'], style: const TextStyle(color: CustomColors.mainDark))
                  )
                )
              ])
            );
          }
        }
        children.add(_segmentActionIcon(InfoPanelTexts.editSchedule, false));
        break;
      case 3:
        if(_business!.services.length > 0) {
          List services = _business!.services.cast();
          for(int i = 0; i < services.length; i++) {
            children.add(
              Container(child: Row(children: [
                _contentExpanded(services[i], Icons.remove),
                _actionsExpanded(InfoPanelTexts.tooltipEditService, InfoPanelTexts.tooltipDeleteService, services, i)
              ]))
            );
          }
        }
        children.add(_segmentActionIcon(InfoPanelTexts.newService, true));
        break;
      case 4:
        if(_business!.contact.length > 0) {
          List contacts = _business!.contact.cast();
          for(int x = 0; x < contacts.length; x++) {
            List mapKeys = contacts[x].keys.toList();
            List mapValues = contacts[x].values.toList();
            for(int y = 0; y < contacts[x].length; y++) {
              childrenValue.add(
                Text('${mapKeys[y]}: ${mapValues[y]}\n', style: const TextStyle(fontSize: 16, color: CustomColors.mainDark))
              );
            }
          }
          children.add(
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: childrenValue
            )
          );
          if(_business!.type == 0 && _business!.adoptionLink != null && _business!.adoptionLink!.isNotEmpty) {
            children.add(Text('Información de adopción: ${_business!.adoptionLink}\n', style: const TextStyle(fontSize: 16, color: CustomColors.mainDark), textAlign: TextAlign.center));
          }
        }
        children.add(_segmentActionIcon(InfoPanelTexts.editContact, false));
        break;
    }

    return children;
  }

  Widget _contentExpanded(String content, IconData icon) {
    return Expanded(
      flex: 3,
      child: ListTile(
        leading: Icon(icon, color: CustomColors.mainGreen),
        title: Text(content)
      )
    );
  }

  Widget _actionsExpanded(String ttEdit, String ttDelete, List list, int itemIndex) {
    return Expanded(
      flex: 2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            icon: Icon(Icons.edit),
            color: CustomColors.mainDark,
            tooltip: ttEdit,
            onPressed: () => _openEditItemForm(itemIndex)
          ),
          IconButton(
            icon: Icon(Icons.delete),
            color: CustomColors.mainDark,
            tooltip: ttDelete,
            onPressed: () => _deleteItem(list, itemIndex)
          )
        ]
      )
    );
  }

  Widget _segmentActionIcon(String tooltip, bool isNew) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      color: CustomColors.mainDark,
      child: IconButton(
        icon: isNew ? Icon(Icons.add) : Icon(Icons.edit),
        color: Colors.white,
        tooltip: tooltip,
        onPressed: () => isNew ? _openNewItemForm() : _openEditSegmentForm()
      )
    );
  }

  void _openNewItemForm() {
    switch(widget.panelIndex) {
      case 0: // ADDRESS
        Navigator.of(context).push(FadeRoute(page: InfoListFormScreen(), arguments: [true, 0, _business, null]));
        break;
      case 1: // PHONE
        Navigator.of(context).push(FadeRoute(page: InfoListFormScreen(), arguments: [true, 1, _business, null]));
        break;
      case 3: // SERVICE
        Navigator.of(context).push(FadeRoute(page: InfoListFormScreen(), arguments: [true, 3, _business, null]));
        break;
    }
  }

  void _openEditItemForm(int itemIndex) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: InfoPanelTexts.editTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: InfoPanelTexts.editBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: InfoPanelTexts.btnCancel,
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: InfoPanelTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                switch(widget.panelIndex) {
                  case 0: // ADDRESS
                    Navigator.of(context).push(FadeRoute(page: InfoListFormScreen(), arguments: [false, 0, _business, itemIndex]));
                    break;
                  case 1: // PHONE
                    Navigator.of(context).push(FadeRoute(page: InfoListFormScreen(), arguments: [false, 1, _business, itemIndex]));
                    break;
                  case 3: // SERVICE
                    Navigator.of(context).push(FadeRoute(page: InfoListFormScreen(), arguments: [false, 3, _business, itemIndex]));
                    break;
                }
              }
            )
          ]
        );
      }
    );
  }

  void _openEditSegmentForm() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: InfoPanelTexts.editTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: InfoPanelTexts.editBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: InfoPanelTexts.btnCancel,
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: InfoPanelTexts.btnEdit,
              pressCallback: () {
                Navigator.of(context).pop();
                switch(widget.panelIndex) {
                  case 2: // SCHEDULE
                    Navigator.of(context).push(FadeRoute(page: SegmentFormScreen(), arguments: [2, _business]));
                    break;
                  case 4: // CONTACT
                    Navigator.of(context).push(FadeRoute(page: SegmentFormScreen(), arguments: [4, _business]));
                    break;
                }
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(List list, int itemIndex) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: InfoPanelTexts.deleteTitle, style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: InfoPanelTexts.deleteBody)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: InfoPanelTexts.btnCancel,
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: InfoPanelTexts.btnDelete,
              pressCallback: () async {
                list.removeAt(itemIndex);
                bool success = await _database.saveBusiness(_business!, false);
                if(success) {
                  Navigator.of(context).pop();
                }
              }
            )
          ]
        );
      }
    );
  }

}

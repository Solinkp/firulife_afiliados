import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../settings/custom_shared_preferences.dart';
import '../models/business.dart';
import './spinner_loader.dart';
import './firu_segment_business.dart';

class Dashboard extends StatelessWidget {
  final  BaseDatabase _database = Database();

  @override
  Widget build(BuildContext context) {
    double pixelRatio = MediaQuery.of(context).devicePixelRatio;

    return StreamBuilder<Business>(
      stream: _database.getLinkedBusiness(),
      builder: (_, AsyncSnapshot snapshot) {
        switch(snapshot.connectionState) {
          case ConnectionState.waiting:
            return _showLoader();
          default:
            Business? business = snapshot.data;
            if(business == null) {
              return _showContentNoData();
            } else {
              _setLocalBusiness(business.id!);
              return _showContentData(pixelRatio, business.type);
            }
        }
      }
    );
  }

  Widget _showLoader() {
    return SpinnerLoader(color: CustomColors.mainGreen);
  }

  Widget _showContentNoData() {
    return Container(
      alignment: Alignment.center,
      child: FiruSegmentBusiness(index: 4, title: 'Crear Registro de Negocio')
    );
  }

  void _setLocalBusiness(String businessId) async {
    await CustomSharedPreferences().setLocalBusiness(businessId);
  }

  Widget _showContentData(double pixelRatio, int businessType) {
    String infoText = 'Información';
    String productText = 'Productos';
    String firusText = 'Adopciones';
    String donationText = 'Donaciones';

    return Container(
      child: ResponsiveGridList(
        rowMainAxisAlignment: MainAxisAlignment.center,
        minSpacing: 20,
        desiredItemWidth: pixelRatio <= 3.0 ? 150 : 130,
        squareCells: true,
        children: [
          // Info
          FiruSegmentBusiness(index: 0, title: infoText),
          // Productos
          FiruSegmentBusiness(index: 1, title: productText),
          // Firus
          businessType == 0 ? FiruSegmentBusiness(index: 2, title: firusText) : Container(height: 0.0, width: 0.0),
          // DonationAcc
          businessType == 0 ? FiruSegmentBusiness(index: 3, title: donationText) : Container(height: 0.0, width: 0.0)
        ]
      )
    );
  }

}

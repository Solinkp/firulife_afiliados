import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function() pressCallback;
  final double textSize;
  final Color textColor;
  final double height;
  final double topPadding;
  final double botPadding;
  final double elevation;

  CustomButton({
    required this.text,
    required this.pressCallback,
    this.textSize = 18.0,
    this.textColor = Colors.white,
    this.height = 40.0,
    this.topPadding = 35.0,
    this.botPadding = 0.0,
    this.elevation = 5.0
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: topPadding, bottom: botPadding),
      child: SizedBox(
        height: height,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            elevation: elevation,
            primary: CustomColors.mainGreen,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0)
            ),
            textStyle: TextStyle(fontSize: textSize, color: textColor)
          ),
          child: Text(text),
          onPressed: pressCallback
        )
      )
    );
  }

}

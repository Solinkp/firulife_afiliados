import 'package:flutter/material.dart';

import '../routing/fade_route.dart';
import '../settings/custom_colors.dart';
import '../screens/business/details/info_screen.dart';
import '../screens/business/details/products_screen.dart';
import '../screens/business/details/pets_screen.dart';
import '../screens/business/details/banks_screen.dart';
import '../screens/business/forms/new_business_form.dart';

class FiruSegmentBusiness extends StatelessWidget {
  final int index;
  final String title;

  FiruSegmentBusiness({required this.index, required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: InkWell(
        onTap: () => goToSelectedSegment(context),
        splashColor: Theme.of(context).accentColor,
        borderRadius: BorderRadius.circular(15),
        child: Container(
          child: Center(child: Text(title, style: TextStyle(color: Colors.white), textAlign: TextAlign.center)),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.5, 0.5],
              colors: [CustomColors.mainGreen.withOpacity(0.85), CustomColors.mainDark.withOpacity(0.85)]
            ),
            borderRadius: BorderRadius.circular(15)
          )
        )
      ),
    );
  }

  void goToSelectedSegment(BuildContext context) {
    switch(index) {
      case 0:
        Navigator.of(context).push(FadeRoute(page: InfoScreen()));
      break;
      case 1:
        Navigator.of(context).push(FadeRoute(page: ProductsScreen()));
      break;
      case 2:
        Navigator.of(context).push(FadeRoute(page: PetsScreen()));
      break;
      case 3:
        Navigator.of(context).push(FadeRoute(page: BanksScreen()));
      break;
      case 4:
        Navigator.of(context).push(FadeRoute(page: NewBusinessForm()));
      break;
    }
  }

}

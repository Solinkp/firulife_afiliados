import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../services/authentication.dart';
import '../screens/auth/login_screen.dart';
import '../screens/business/business_dashboard.dart';
import '../screens/loading_screen.dart';

enum AuthStatus { NOT_DETERMINED, NOT_LOGGED_IN, LOGGED_IN }

class RootWidget extends StatefulWidget {
  @override
  _RootWidgetState createState() => _RootWidgetState();
}

class _RootWidgetState extends State<RootWidget> {
  final BaseAuth auth = Auth();
  AuthStatus _authStatus = AuthStatus.NOT_DETERMINED;
  late int _userType;

  @override
  Widget build(BuildContext context) {
    User? _user = Provider.of<User?>(context);

    if (_user != null) {
      if (_authStatus != AuthStatus.LOGGED_IN) {
        _authStatus = AuthStatus.NOT_DETERMINED;
        auth.getUserTypeClaim(_user).then((claims) async {
          setState(() {
            _userType = claims;
            _authStatus = AuthStatus.LOGGED_IN;
          });
        });
      }
    } else {
      _authStatus = AuthStatus.NOT_LOGGED_IN;
    }

    switch (_authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return LoadingScreen();
      case AuthStatus.NOT_LOGGED_IN:
        return LoginScreen();
      case AuthStatus.LOGGED_IN:
        switch (_userType) {
          case 0:
            auth.signOut();
            return LoginScreen();
          case 1:
            return BusinessDashboard();
          case 2:
            auth.signOut();
            return LoginScreen();
          default:
            auth.signOut();
            return LoginScreen();
        }
      default:
        return LoginScreen();
    }
  }
}

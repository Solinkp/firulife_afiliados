import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader extends StatelessWidget {
  final bool visible;

  const Loader({
    Key? key,
    required this.visible,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return visible
        ? Center(
            child: SpinKitCircle(
              color: Colors.amber,
              size: 120.0,
            ),
          )
        : Container();
  }
}

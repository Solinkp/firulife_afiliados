import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFormField extends StatefulWidget {
  final TextInputType inputType;
  final String hintText;
  final IconData icon;
  final int index;
  final Function(int, String) setValueForm;
  final Function(int, dynamic) validatorFunction;
  final TextEditingController textController;
  final String? helperText;
  final bool obscured;
  final bool last;
  final bool theme;
  final int lines;
  final bool isNumber;
  final int? charLength;
  final bool isReadOnly;
  final Function()? onTap;

  CustomTextFormField(
      {required this.inputType,
      required this.hintText,
      required this.icon,
      required this.index,
      required this.setValueForm,
      required this.validatorFunction,
      required this.textController,
      this.helperText,
      this.obscured = false,
      this.last = false,
      this.theme = true,
      this.lines = 1,
      this.isNumber = false,
      this.charLength,
      this.isReadOnly = false,
      this.onTap});

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _passwordInvisible = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: widget.index == 0
            ? const EdgeInsets.all(0.0)
            : const EdgeInsets.only(top: 15.0),
        child: TextFormField(
          maxLines: widget.lines,
          maxLength: widget.charLength,
          keyboardType: widget.inputType,
          obscureText: widget.obscured ? _passwordInvisible : widget.obscured,
          autofocus: false,
          cursorColor: widget.theme ? Colors.white : CustomColors.mainDark,
          decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: widget.theme
                          ? Colors.white
                          : CustomColors.mainGreen)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  borderSide: BorderSide(
                      color: widget.theme
                          ? CustomColors.secondaryGreen
                          : CustomColors.mainGreen)),
              errorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: widget.theme ? Colors.limeAccent : Colors.red)),
              focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  borderSide: BorderSide(
                      color: widget.theme ? Colors.limeAccent : Colors.red)),
              errorStyle: TextStyle(
                  color: widget.theme ? Colors.limeAccent : Colors.red),
              hintText: widget.hintText,
              hintStyle: TextStyle(
                  color: widget.theme ? Colors.white : CustomColors.mainDark),
              helperText: widget.helperText,
              icon: Icon(widget.icon,
                  color: widget.theme ? Colors.white : CustomColors.mainDark),
              suffixIcon: widget.obscured
                  ? IconButton(
                      icon: Icon(
                          _passwordInvisible
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: widget.theme
                              ? Colors.white
                              : CustomColors.mainDark),
                      onPressed: () => setState(
                          () => _passwordInvisible = !_passwordInvisible))
                  : null),
          style: TextStyle(
              color: widget.theme ? Colors.white : CustomColors.mainDark),
          controller: widget.textController,
          inputFormatters: widget.isNumber
              ? <TextInputFormatter>[
                  FilteringTextInputFormatter.allow(RegExp(r'(^\-?\d*\.?\d*)'))
                ]
              : null,
          validator: (value) => widget.validatorFunction(widget.index, value),
          onSaved: (value) => widget.setValueForm(widget.index, value!),
          textInputAction:
              widget.last ? TextInputAction.done : TextInputAction.next,
          readOnly: widget.isReadOnly,
          onTap: widget.onTap,
        ));
  }
}

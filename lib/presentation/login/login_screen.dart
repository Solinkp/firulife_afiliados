import 'package:firulife_afiliados/presentation/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:firulife_afiliados/presentation/widgets/custom_safe_area.dart';

// final _authProvider = Provider.autoDispose((ref) => ref.watch(authProvider));

class LOginScreen extends StatefulWidget {
  const LOginScreen({Key? key}) : super(key: key);

  @override
  State<LOginScreen> createState() => _LOginScreenState();
}

class _LOginScreenState extends State<LOginScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  final _isLoading = false;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomSafeArea(
      child: Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text('Firulife', style: Theme.of(context).textTheme.headline6),
        ),
        body: Stack(
          children: [
            Loader(
              visible: _isLoading,
            ),
            Form(
              key: _formKey,
              child: Container(
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

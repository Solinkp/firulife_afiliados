import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:firulife_afiliados/presentation/dashboard/dashboard_screen.dart';
import 'package:firulife_afiliados/presentation/signup/signup_screen.dart';
import 'package:firulife_afiliados/screens/auth/login_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route,Screen',
  routes: [
    AutoRoute(
      path: '/',
      name: 'login',
      page: LoginScreen,
    ),
    AutoRoute(
      path: '/signup',
      name: 'signUp',
      page: SignUpScreen,
    ),
    AutoRoute(
      path: '/dashboard',
      name: 'dashboard',
      page: DashboardScreen,
    ),
  ],
)
class $AppRouter {}
